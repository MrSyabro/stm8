#include "motor.h"
#include "main.h"

static uint8_t curent_pin = 1;

static uint8_t pins[] = {MIN1_Pin, MIN2_Pin, MIN3_Pin, MIN4_Pin};

void motor_init()
{
	M_Port->DDR |= MIN_Mask;
	M_Port->CR1 |= MIN_Mask;
	//M_Port->CR2 |= MIN_Mask;
}

uint8_t next_pin(uint8_t pin, int8_t dir)
{
	if (dir > 0)
	{
		pin++;
		if (pin > 4) pin = 1;
	}
	else if (dir < 0)
	{
		pin--;
		if (pin < 1 || pin > 4) pin = 4;
	}
	return pin;
}

/* uint8_t next_to_step_pin (uint8_t pin, uint8_t dir) */
/* { */
/* 	if (dir) */
/* 	{ */
/* 		pin++; */
/* 		if (pin > 4) pin = 1; */
/* 	} */
/* 	else */
/* 	{ */
/* 		pin--; */
/* 		if (pin < 1) pin = 4; */
/* 	} */
/* 	return pin; */
/* } */


void motor_step(int8_t dir, uint8_t speed)
{
	int tick = 10000 - speed * 1000;
	M_Port->ODR |= pins[curent_pin-1];
	delay(tick);
	M_Port->ODR &= ~(pins[next_pin (curent_pin, dir*(-1))-1]);
	delay(tick);
	curent_pin = next_pin (curent_pin, dir);
	
	
	/*if (dir > 0){
		M_Port->ODR |= MIN1_Pin;
		delay (tick);
		M_Port->ODR &= ~MIN4_Pin;
		delay (tick);
		M_Port->ODR |= MIN2_Pin;
		delay (tick);
		M_Port->ODR &= ~MIN1_Pin;
		delay (tick);
		M_Port->ODR |= MIN3_Pin;
		delay (tick);
		M_Port->ODR &= ~MIN2_Pin;
		delay (tick);
		M_Port->ODR |= MIN4_Pin;
		delay (tick);
		M_Port->ODR &= ~MIN3_Pin;
		delay (tick);
	}else if (dir < 0){
		M_Port->ODR |= MIN4_Pin;
		delay (tick);
		M_Port->ODR &= ~MIN1_Pin;
		delay (tick);
		M_Port->ODR |= MIN3_Pin;
		delay (tick);
		M_Port->ODR &= ~MIN4_Pin;
		delay (tick);
		M_Port->ODR |= MIN2_Pin;
		delay (tick);
		M_Port->ODR &= ~MIN3_Pin;
		delay (tick);
		M_Port->ODR |= MIN1_Pin;
		delay (tick);
		M_Port->ODR &= ~MIN2_Pin;
		delay (tick);
	}*/
}

void motor_turn(int steps, int speed)
{
	int8_t s = 1;
	if (steps < 0) s = -1;
	for (int i = 0; i != steps; i + s)
	{
		motor_step(s, speed);
	}
}
		
void motor_stop(){
	M_Port->ODR &= ~(MIN_Mask);
}

#include "main.h"
#include "rc522.h"
#include "spi.h"
#include "periph.h"

void rc_init ()
{
	printf("Init RC\n\r");
	
	//RST_Port->DDR |= RST_Pin;
	SDA_Port->DDR |= SDA_Pin;
	
	//RST_Port->CR2 |= RST_Pin;
	//SDA_Port->CR2 |= SDA_Pin;
	
	RST_Port->CR1 |= RST_Pin;
	SDA_Port->CR1 |= SDA_Pin;
	
	//RST_Port->ODR |= RST_Pin;
	
	uint8_t i = (uint8_t)1000000U;
	while(i--);
	UNSEL_RC;
	spi_init ();
	
	// Init RC522
	
	rc_write_reg (0x2A, 0x8D);
	rc_write_reg (0x2B, 0x3E);
	
	rc_write_reg (0x2D, 30);
	rc_write_reg (0x2C, 0);
	
	rc_write_reg (0x15, 0x40); // 100% ASK
	rc_write_reg (0x11, 0x29); // Enable CRC, polarity of pin MFIN is active HIGH
	
	rc_set_mask (0x14, 0x3); // Enable antena
}

uint8_t rc_read_reg(uint8_t adr)
{
	SEL_RC;
	spi_rw (adr);
	uint8_t data = spi_rw (0);
	UNSEL_RC;
	
	return data;
}

void rc_write_reg (uint8_t adr, uint8_t data)
{
	SEL_RC;
	spi_rw (adr);
	spi_rw (data);
	UNSEL_RC;
}

void rc_set_mask (uint8_t adr, uint8_t mask)
{
	SEL_RC;
	spi_rw (adr);
	uint8_t data = spi_rw (0x0);
	spi_rw (adr);
	spi_rw (data | mask);
	UNSEL_RC;
}

void rc_clr_mask (uint8_t adr, uint8_t mask)
{
	SEL_RC;
	spi_rw (adr);
	uint8_t data = spi_rw (0x0);
	spi_rw (adr);
	spi_rw (data & ~mask);
	UNSEL_RC;
}

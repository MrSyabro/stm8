#include "uart.h"
#include "periph.h"

void uart_init()
{
	CLK->PCKENR1 |= 0b1100;
	
	UART->BRR1 = 0x68;
	UART->BRR2 = 0x03;
	
	UART->CR2 = 0x8;
}

//void uart_print ()

int putchar (int data)
{
	uart_w ((uint8_t)data);
	return data;
}

void uart_w (uint8_t data)
{
	while(!(UART->SR & 0x80));
	UART->DR = data;
}

uint8_t uart_r ()
{
	uint8_t data = UART->DR;
	return data;
}

#include "sh_reg.h"

void sr_init()
{
	SR_CS_Port->DDR |= SR_CS_Pin;
	SR_CS_Port->CR2 |= SR_CS_Pin;
	SR_CS_Port->CR1 |= SR_CS_Pin;
	spi_init ();
}

void sr_wout(uint8_t out)
{
	spi_rw(out);
	H_CS;
	L_CS;
}

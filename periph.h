#include <stdint.h>

#ifndef PERIPH_H
#define PERIPH_H
#define     __IO    volatile

typedef struct GPIO_struct
{
	__IO uint8_t ODR;
	__IO uint8_t IDR;
	__IO uint8_t DDR;
	__IO uint8_t CR1;
	__IO uint8_t CR2;
}
GPIO_TypeDef;

typedef struct SPI_struct
{
	__IO uint8_t CR1;
	__IO uint8_t CR2;
	__IO uint8_t ICR;
	__IO uint8_t SR;
	__IO uint8_t DR;
	__IO uint8_t CRCPR;
	__IO uint8_t RXCRCR;
	__IO uint8_t TXCRCR;
}
SPI_TypeDef;

typedef struct CLK_struct
{
	__IO uint8_t ICKR;
	__IO uint8_t ECKR;
	uint8_t RESERVED;
	__IO uint8_t CMSR;
	__IO uint8_t SWR;
	__IO uint8_t SWCR;
	__IO uint8_t CKDIVR;
	__IO uint8_t PCKENR1;
	__IO uint8_t CSSR;
	__IO uint8_t CCOR;
	__IO uint16_t PCKENR2;
	__IO uint8_t HSITRIMR;
	__IO uint8_t SWIMCCR;
}
CLK_TypeDef;

typedef struct UART_struct
{
	__IO uint8_t SR;
	__IO uint8_t DR;
	__IO uint8_t BRR1;
	__IO uint8_t BRR2;
	__IO uint8_t CR1;
	__IO uint8_t CR2;
	__IO uint8_t CR3;
	__IO uint8_t CR4;
	__IO uint8_t CR5;
	__IO uint8_t GTR;
	__IO uint8_t PSCR;
}
UART_TypeDef;

typedef struct I2C_struct
{
	__IO uint8_t CR1;
	__IO uint8_t CR2;
	__IO uint8_t FREQR;
	__IO uint8_t OARL;
	__IO uint8_t OARH;
	uint8_t RESERVED;
	__IO uint8_t DR;
	__IO uint8_t SR1;
	__IO uint8_t SR2;
	__IO uint8_t SR3;
	__IO uint8_t ITR;
	__IO uint8_t CCRL;
	__IO uint8_t CCRH;
	__IO uint8_t TRISER;
	__IO uint8_t PECR;
}
I2C_TypeDef;

#define GPIOA_BaseAddress		0x5000
#define GPIOB_BaseAddress		0x5005
#define GPIOC_BaseAddress   	0x500A
#define GPIOD_BaseAddress		0x500F
#define SPI_BaseAddress			0x5200
#define CLK_BaseAddress			0x50C0
#define UART_BaseAddress		0x5230
#define I2C_BaseAdress			0x5210

#define CLK		((CLK_TypeDef *)	CLK_BaseAddress)
#define GPIOA	((GPIO_TypeDef *)	GPIOA_BaseAddress)
#define GPIOB	((GPIO_TypeDef *)	GPIOB_BaseAddress)
#define GPIOC	((GPIO_TypeDef *)	GPIOC_BaseAddress)
#define GPIOD	((GPIO_TypeDef *)	GPIOD_BaseAddress)
#define SPI		((SPI_TypeDef *)	SPI_BaseAddress)
#define UART	((UART_TypeDef *)	UART_BaseAddress)
#define I2C		((I2C_TypeDef *)	I2C_BaseAdress)

#endif

#include <stdint.h>

#ifndef RC522_H
#define RC522_H

void rc_init ();
uint8_t rc_read_reg(uint8_t adr);
void rc_write_reg (uint8_t adr, uint8_t data);
void rc_set_mask (uint8_t adr, uint8_t mask);
void rc_clr_mask (uint8_t adr, uint8_t mask);

#define RST_Port GPIOC
#define SDA_Port GPIOB

#define RST_Pin 0b10000
#define SDA_Pin 0b10000 // 🤣️

#define SEL_RC (SDA_Port->ODR &= ~SDA_Pin)
#define UNSEL_RC (SDA_Port->ODR |= SDA_Pin)

#endif

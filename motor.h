#include "periph.h"

#ifndef MOTOR_H
#define MOTOR_H

#define M_Port GPIOC
#define MIN1_Pin 0b1000
#define MIN2_Pin 0b10000
#define MIN3_Pin 0b100000
#define MIN4_Pin 0b1000000
#define MIN_Mask 0b1111000

void motor_init();
void motor_step(int8_t dir, uint8_t speed);
void motor_turn(int steps, int speed);
void motor_stop();

#endif

#include <stdint.h>

#ifndef UART_H
#define UART_H

void uart_init ();
void uart_w (uint8_t data);
uint8_t uart_r ();
int putchar (int data);

#endif

#include <stdint.h>

#ifndef I2C_H
#define I2C_H

void i2c_init();
void i2c_send_adr(uint8_t adr);
void i2c_start ();
void i2c_stop ();
void i2c_send (uint8_t data);
uint8_t i2c_recv ();

#endif

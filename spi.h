#include <stdint.h>

#ifndef SPI_H
#define SPI_H

void spi_init ();
uint8_t spi_rw (uint8_t data);

#endif

;--------------------------------------------------------
; File Created by SDCC : free open source ANSI-C Compiler
; Version 3.9.0 #11195 (Linux)
;--------------------------------------------------------
	.module sh_reg
	.optsdcc -mstm8
	
;--------------------------------------------------------
; Public variables in this module
;--------------------------------------------------------
	.globl _spi_rw
	.globl _spi_init
	.globl _sr_init
	.globl _sr_wout
;--------------------------------------------------------
; ram data
;--------------------------------------------------------
	.area DATA
;--------------------------------------------------------
; ram data
;--------------------------------------------------------
	.area INITIALIZED
;--------------------------------------------------------
; absolute external ram data
;--------------------------------------------------------
	.area DABS (ABS)

; default segment ordering for linker
	.area HOME
	.area GSINIT
	.area GSFINAL
	.area CONST
	.area INITIALIZER
	.area CODE

;--------------------------------------------------------
; global & static initialisations
;--------------------------------------------------------
	.area HOME
	.area GSINIT
	.area GSFINAL
	.area GSINIT
;--------------------------------------------------------
; Home
;--------------------------------------------------------
	.area HOME
	.area HOME
;--------------------------------------------------------
; code
;--------------------------------------------------------
	.area CODE
	G$sr_init$0$0 ==.
	C$sh_reg.c$3$0_0$3 ==.
;	sh_reg.c: 3: void sr_init()
; genLabel
;	-----------------------------------------
;	 function sr_init
;	-----------------------------------------
;	Register assignment is optimal.
;	Stack space usage: 0 bytes.
_sr_init:
	C$sh_reg.c$5$1_0$3 ==.
;	sh_reg.c: 5: SR_CS_Port->DDR |= SR_CS_Pin;
; genPointerGet
; genOr
; genPointerSet
	bset	20497, #1
; peephole 18-1 replaced or by bset.
	C$sh_reg.c$6$1_0$3 ==.
;	sh_reg.c: 6: SR_CS_Port->CR2 |= SR_CS_Pin;
; genPointerGet
; genOr
; genPointerSet
	bset	20499, #1
; peephole 18-1 replaced or by bset.
	C$sh_reg.c$7$1_0$3 ==.
;	sh_reg.c: 7: SR_CS_Port->CR1 |= SR_CS_Pin;
; genPointerGet
; genOr
; genPointerSet
	bset	20498, #1
; peephole 18-1 replaced or by bset.
	C$sh_reg.c$8$1_0$3 ==.
;	sh_reg.c: 8: spi_init ();
; genCall
	call	_spi_init
; genLabel
; peephole j30 removed unused label 00101$.
	C$sh_reg.c$9$1_0$3 ==.
;	sh_reg.c: 9: }
; genEndFunction
	C$sh_reg.c$9$1_0$3 ==.
	XG$sr_init$0$0 ==.
	ret
	G$sr_wout$0$0 ==.
	C$sh_reg.c$11$1_0$5 ==.
;	sh_reg.c: 11: void sr_wout(uint8_t out)
; genLabel
;	-----------------------------------------
;	 function sr_wout
;	-----------------------------------------
;	Register assignment is optimal.
;	Stack space usage: 0 bytes.
_sr_wout:
	C$sh_reg.c$13$1_0$5 ==.
;	sh_reg.c: 13: spi_rw(out);
; genIPush
	ld	a, (0x03, sp)
	push	a
; genCall
	call	_spi_rw
	pop	a
	C$sh_reg.c$14$1_0$5 ==.
;	sh_reg.c: 14: H_CS;
; genPointerGet
; genOr
; genPointerSet
	bset	20495, #1
; peephole 18-1 replaced or by bset.
	C$sh_reg.c$15$1_0$5 ==.
;	sh_reg.c: 15: L_CS;
; genPointerGet
; genAnd
; genPointerSet
	bres	20495, #1
; peephole 19-1 replaced and by bres.
; genLabel
; peephole j30 removed unused label 00101$.
	C$sh_reg.c$16$1_0$5 ==.
;	sh_reg.c: 16: }
; genEndFunction
	C$sh_reg.c$16$1_0$5 ==.
	XG$sr_wout$0$0 ==.
	ret
	.area CODE
	.area CONST
	.area INITIALIZER
	.area CABS (ABS)

                                      1 ;--------------------------------------------------------
                                      2 ; File Created by SDCC : free open source ANSI-C Compiler
                                      3 ; Version 3.9.0 #11195 (Linux)
                                      4 ;--------------------------------------------------------
                                      5 	.module main
                                      6 	.optsdcc -mstm8
                                      7 	
                                      8 ;--------------------------------------------------------
                                      9 ; Public variables in this module
                                     10 ;--------------------------------------------------------
                                     11 	.globl _main
                                     12 	.globl _sr_wout
                                     13 	.globl _sr_init
                                     14 	.globl _uart_init
                                     15 	.globl _printf
                                     16 	.globl _delay
                                     17 ;--------------------------------------------------------
                                     18 ; ram data
                                     19 ;--------------------------------------------------------
                                     20 	.area DATA
                                     21 ;--------------------------------------------------------
                                     22 ; ram data
                                     23 ;--------------------------------------------------------
                                     24 	.area INITIALIZED
                                     25 ;--------------------------------------------------------
                                     26 ; Stack segment in internal ram 
                                     27 ;--------------------------------------------------------
                                     28 	.area	SSEG
      FFFFFF                         29 __start__stack:
      FFFFFF                         30 	.ds	1
                                     31 
                                     32 ;--------------------------------------------------------
                                     33 ; absolute external ram data
                                     34 ;--------------------------------------------------------
                                     35 	.area DABS (ABS)
                                     36 
                                     37 ; default segment ordering for linker
                                     38 	.area HOME
                                     39 	.area GSINIT
                                     40 	.area GSFINAL
                                     41 	.area CONST
                                     42 	.area INITIALIZER
                                     43 	.area CODE
                                     44 
                                     45 ;--------------------------------------------------------
                                     46 ; interrupt vector 
                                     47 ;--------------------------------------------------------
                                     48 	.area HOME
      008000                         49 __interrupt_vect:
      008000 82 00 80 07             50 	int s_GSINIT ; reset
                                     51 ;--------------------------------------------------------
                                     52 ; global & static initialisations
                                     53 ;--------------------------------------------------------
                                     54 	.area HOME
                                     55 	.area GSINIT
                                     56 	.area GSFINAL
                                     57 	.area GSINIT
      008007                         58 __sdcc_gs_init_startup:
      008007                         59 __sdcc_init_data:
                                     60 ; stm8_genXINIT() start
      008007 AE 00 00         [ 2]   61 	ldw x, #l_DATA
      00800A 27 07            [ 1]   62 	jreq	00002$
      00800C                         63 00001$:
      00800C 72 4F 00 00      [ 1]   64 	clr (s_DATA - 1, x)
      008010 5A               [ 2]   65 	decw x
      008011 26 F9            [ 1]   66 	jrne	00001$
      008013                         67 00002$:
      008013 AE 00 01         [ 2]   68 	ldw	x, #l_INITIALIZER
      008016 27 09            [ 1]   69 	jreq	00004$
      008018                         70 00003$:
      008018 D6 80 50         [ 1]   71 	ld	a, (s_INITIALIZER - 1, x)
      00801B D7 00 00         [ 1]   72 	ld	(s_INITIALIZED - 1, x), a
      00801E 5A               [ 2]   73 	decw	x
      00801F 26 F7            [ 1]   74 	jrne	00003$
      008021                         75 00004$:
                                     76 ; stm8_genXINIT() end
                                     77 	.area GSFINAL
      008021 CC 80 04         [ 2]   78 	jp	__sdcc_program_startup
                                     79 ;--------------------------------------------------------
                                     80 ; Home
                                     81 ;--------------------------------------------------------
                                     82 	.area HOME
                                     83 	.area HOME
      008004                         84 __sdcc_program_startup:
      008004 CC 80 83         [ 2]   85 	jp	_main
                                     86 ;	return from main will return to caller
                                     87 ;--------------------------------------------------------
                                     88 ; code
                                     89 ;--------------------------------------------------------
                                     90 	.area CODE
                           000000    91 	G$delay$0$0 ==.
                           000000    92 	C$main.c$9$0_0$17 ==.
                                     93 ;	main.c: 9: void delay(uint32_t t)
                                     94 ; genLabel
                                     95 ;	-----------------------------------------
                                     96 ;	 function delay
                                     97 ;	-----------------------------------------
                                     98 ;	Register assignment might be sub-optimal.
                                     99 ;	Stack space usage: 8 bytes.
      008052                        100 _delay:
      008052 52 08            [ 2]  101 	sub	sp, #8
                           000002   102 	C$main.c$11$1_0$17 ==.
                                    103 ;	main.c: 11: while(t--) {}
                                    104 ; genAssign
      008054 16 0D            [ 2]  105 	ldw	y, (0x0d, sp)
      008056 17 07            [ 2]  106 	ldw	(0x07, sp), y
      008058 16 0B            [ 2]  107 	ldw	y, (0x0b, sp)
      00805A 17 05            [ 2]  108 	ldw	(0x05, sp), y
                                    109 ; genLabel
      00805C                        110 00101$:
                                    111 ; genAssign
      00805C 1E 07            [ 2]  112 	ldw	x, (0x07, sp)
      00805E 1F 03            [ 2]  113 	ldw	(0x03, sp), x
      008060 1E 05            [ 2]  114 	ldw	x, (0x05, sp)
      008062 1F 01            [ 2]  115 	ldw	(0x01, sp), x
                                    116 ; genMinus
      008064 16 07            [ 2]  117 	ldw	y, (0x07, sp)
      008066 72 A2 00 01      [ 2]  118 	subw	y, #0x0001
      00806A 17 07            [ 2]  119 	ldw	(0x07, sp), y
      00806C 7B 06            [ 1]  120 	ld	a, (0x06, sp)
      00806E A2 00            [ 1]  121 	sbc	a, #0x00
      008070 6B 06            [ 1]  122 	ld	(0x06, sp), a
      008072 7B 05            [ 1]  123 	ld	a, (0x05, sp)
      008074 A2 00            [ 1]  124 	sbc	a, #0x00
      008076 6B 05            [ 1]  125 	ld	(0x05, sp), a
                                    126 ; genIfx
      008078 1E 03            [ 2]  127 	ldw	x, (0x03, sp)
      00807A 26 E0            [ 1]  128 	jrne	00101$
                                    129 ; peephole j22 jumped to 00101$ directly instead of via 00112$.
      00807C 1E 01            [ 2]  130 	ldw	x, (0x01, sp)
                                    131 ; peephole j30 removed unused label 00112$.
                                    132 ; peephole j5 changed absolute to relative unconditional jump.
      00807E 26 DC            [ 1]  133 	jrne	00101$
                                    134 ; peephole j7 removed jra by using inverse jump logic
                                    135 ; peephole j30 removed unused label 00113$.
                                    136 ; genLabel
                                    137 ; peephole j30 removed unused label 00104$.
                           00002E   138 	C$main.c$12$1_0$17 ==.
                                    139 ;	main.c: 12: }
                                    140 ; genEndFunction
      008080 5B 08            [ 2]  141 	addw	sp, #8
                           000030   142 	C$main.c$12$1_0$17 ==.
                           000030   143 	XG$delay$0$0 ==.
      008082 81               [ 4]  144 	ret
                           000031   145 	G$main$0$0 ==.
                           000031   146 	C$main.c$14$1_0$20 ==.
                                    147 ;	main.c: 14: int main(void)
                                    148 ; genLabel
                                    149 ;	-----------------------------------------
                                    150 ;	 function main
                                    151 ;	-----------------------------------------
                                    152 ;	Register assignment is optimal.
                                    153 ;	Stack space usage: 0 bytes.
      008083                        154 _main:
                           000031   155 	C$main.c$16$1_0$20 ==.
                                    156 ;	main.c: 16: CLK->CKDIVR = 0x0;
                                    157 ; genPointerSet
      008083 35 00 50 C6      [ 1]  158 	mov	0x50c6+0, #0x00
                           000035   159 	C$main.c$18$1_0$20 ==.
                                    160 ;	main.c: 18: GPIOB->DDR |= (1 << 5);
                                    161 ; genPointerGet
                                    162 ; genOr
                                    163 ; genPointerSet
      008087 72 1A 50 07      [ 1]  164 	bset	20487, #5
                                    165 ; peephole 18-5 replaced or by bset.
                           000039   166 	C$main.c$20$1_0$20 ==.
                                    167 ;	main.c: 20: uart_init ();
                                    168 ; genCall
      00808B CD 80 D0         [ 4]  169 	call	_uart_init
                           00003C   170 	C$main.c$21$1_0$20 ==.
                                    171 ;	main.c: 21: printf("UART init: OK!\n\r");
                                    172 ; skipping iCode since result will be rematerialized
                                    173 ; skipping iCode since result will be rematerialized
                                    174 ; genIPush
      00808E 4B 24            [ 1]  175 	push	#<(___str_0 + 0)
      008090 4B 80            [ 1]  176 	push	#((___str_0 + 0) >> 8)
                                    177 ; genCall
      008092 CD 81 8C         [ 4]  178 	call	_printf
      008095 5B 02            [ 2]  179 	addw	sp, #2
                           000045   180 	C$main.c$23$1_0$20 ==.
                                    181 ;	main.c: 23: delay(10000UL);
                                    182 ; genIPush
      008097 4B 10            [ 1]  183 	push	#0x10
      008099 4B 27            [ 1]  184 	push	#0x27
      00809B 5F               [ 1]  185 	clrw	x
      00809C 89               [ 2]  186 	pushw	x
                                    187 ; genCall
      00809D CD 80 52         [ 4]  188 	call	_delay
      0080A0 5B 04            [ 2]  189 	addw	sp, #4
                           000050   190 	C$main.c$49$1_0$20 ==.
                                    191 ;	main.c: 49: sr_init ();
                                    192 ; genCall
      0080A2 CD 80 FF         [ 4]  193 	call	_sr_init
                           000053   194 	C$main.c$50$1_0$20 ==.
                                    195 ;	main.c: 50: sr_wout (0b0001);
                                    196 ; genIPush
      0080A5 4B 01            [ 1]  197 	push	#0x01
                                    198 ; genCall
      0080A7 CD 81 0F         [ 4]  199 	call	_sr_wout
      0080AA 84               [ 1]  200 	pop	a
                           000059   201 	C$main.c$52$1_0$20 ==.
                                    202 ;	main.c: 52: while(1)
                                    203 ; genLabel
      0080AB                        204 00102$:
                           000059   205 	C$main.c$54$2_0$21 ==.
                                    206 ;	main.c: 54: GPIOB->ODR |= (1 << 5);
                                    207 ; genPointerGet
                                    208 ; genOr
                                    209 ; genPointerSet
      0080AB 72 1A 50 05      [ 1]  210 	bset	20485, #5
                                    211 ; peephole 18-5 replaced or by bset.
                           00005D   212 	C$main.c$55$2_0$21 ==.
                                    213 ;	main.c: 55: delay(100000UL);
                                    214 ; genIPush
      0080AF 4B A0            [ 1]  215 	push	#0xa0
      0080B1 4B 86            [ 1]  216 	push	#0x86
      0080B3 4B 01            [ 1]  217 	push	#0x01
      0080B5 4B 00            [ 1]  218 	push	#0x00
                                    219 ; genCall
      0080B7 CD 80 52         [ 4]  220 	call	_delay
      0080BA 5B 04            [ 2]  221 	addw	sp, #4
                           00006A   222 	C$main.c$56$2_0$21 ==.
                                    223 ;	main.c: 56: GPIOB->ODR &= ~(1 << 5);
                                    224 ; genPointerGet
                                    225 ; genAnd
                                    226 ; genPointerSet
      0080BC 72 1B 50 05      [ 1]  227 	bres	20485, #5
                                    228 ; peephole 19-5 replaced and by bres.
                           00006E   229 	C$main.c$57$2_0$21 ==.
                                    230 ;	main.c: 57: delay(100000UL);
                                    231 ; genIPush
      0080C0 4B A0            [ 1]  232 	push	#0xa0
      0080C2 4B 86            [ 1]  233 	push	#0x86
      0080C4 4B 01            [ 1]  234 	push	#0x01
      0080C6 4B 00            [ 1]  235 	push	#0x00
                                    236 ; genCall
      0080C8 CD 80 52         [ 4]  237 	call	_delay
      0080CB 5B 04            [ 2]  238 	addw	sp, #4
                                    239 ; genGoto
      0080CD 20 DC            [ 2]  240 	jra	00102$
                                    241 ; peephole j5 changed absolute to relative unconditional jump.
                                    242 ; genLabel
                                    243 ; peephole j30 removed unused label 00104$.
                           00007D   244 	C$main.c$59$1_0$20 ==.
                                    245 ;	main.c: 59: }
                                    246 ; genEndFunction
                           00007D   247 	C$main.c$59$1_0$20 ==.
                           00007D   248 	XG$main$0$0 ==.
      0080CF 81               [ 4]  249 	ret
                                    250 	.area CODE
                                    251 	.area CONST
                           000000   252 Fmain$__str_0$0_0$0 == .
                                    253 	.area CONST
      008024                        254 ___str_0:
      008024 55 41 52 54 20 69 6E   255 	.ascii "UART init: OK!"
             69 74 3A 20 4F 4B 21
      008032 0A                     256 	.db 0x0a
      008033 0D                     257 	.db 0x0d
      008034 00                     258 	.db 0x00
                                    259 	.area CODE
                                    260 	.area INITIALIZER
                                    261 	.area CABS (ABS)

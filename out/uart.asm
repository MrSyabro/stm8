;--------------------------------------------------------
; File Created by SDCC : free open source ANSI-C Compiler
; Version 3.9.0 #11195 (Linux)
;--------------------------------------------------------
	.module uart
	.optsdcc -mstm8
	
;--------------------------------------------------------
; Public variables in this module
;--------------------------------------------------------
	.globl _uart_init
	.globl _putchar
	.globl _uart_w
	.globl _uart_r
;--------------------------------------------------------
; ram data
;--------------------------------------------------------
	.area DATA
;--------------------------------------------------------
; ram data
;--------------------------------------------------------
	.area INITIALIZED
;--------------------------------------------------------
; absolute external ram data
;--------------------------------------------------------
	.area DABS (ABS)

; default segment ordering for linker
	.area HOME
	.area GSINIT
	.area GSFINAL
	.area CONST
	.area INITIALIZER
	.area CODE

;--------------------------------------------------------
; global & static initialisations
;--------------------------------------------------------
	.area HOME
	.area GSINIT
	.area GSFINAL
	.area GSINIT
;--------------------------------------------------------
; Home
;--------------------------------------------------------
	.area HOME
	.area HOME
;--------------------------------------------------------
; code
;--------------------------------------------------------
	.area CODE
	G$uart_init$0$0 ==.
	C$uart.c$4$0_0$3 ==.
;	uart.c: 4: void uart_init()
; genLabel
;	-----------------------------------------
;	 function uart_init
;	-----------------------------------------
;	Register assignment is optimal.
;	Stack space usage: 0 bytes.
_uart_init:
	C$uart.c$6$1_0$3 ==.
;	uart.c: 6: CLK->PCKENR1 |= 0b1100;
; genPointerGet
	ld	a, 0x50c7
; genOr
	or	a, #0x0c
; genPointerSet
	ld	0x50c7, a
	C$uart.c$8$1_0$3 ==.
;	uart.c: 8: UART->BRR1 = 0x68;
; genPointerSet
	mov	0x5232+0, #0x68
	C$uart.c$9$1_0$3 ==.
;	uart.c: 9: UART->BRR2 = 0x03;
; genPointerSet
	mov	0x5233+0, #0x03
	C$uart.c$11$1_0$3 ==.
;	uart.c: 11: UART->CR2 = 0x8;
; genPointerSet
	mov	0x5235+0, #0x08
; genLabel
; peephole j30 removed unused label 00101$.
	C$uart.c$12$1_0$3 ==.
;	uart.c: 12: }
; genEndFunction
	C$uart.c$12$1_0$3 ==.
	XG$uart_init$0$0 ==.
	ret
	G$putchar$0$0 ==.
	C$uart.c$16$1_0$5 ==.
;	uart.c: 16: int putchar (int data)
; genLabel
;	-----------------------------------------
;	 function putchar
;	-----------------------------------------
;	Register assignment is optimal.
;	Stack space usage: 0 bytes.
_putchar:
	C$uart.c$18$1_0$5 ==.
;	uart.c: 18: uart_w ((uint8_t)data);
; genCast
; genAssign
	ld	a, (0x04, sp)
; genIPush
	push	a
; genCall
	call	_uart_w
	pop	a
	C$uart.c$19$1_0$5 ==.
;	uart.c: 19: return data;
; genReturn
	ldw	x, (0x03, sp)
; genLabel
; peephole j30 removed unused label 00101$.
	C$uart.c$20$1_0$5 ==.
;	uart.c: 20: }
; genEndFunction
	C$uart.c$20$1_0$5 ==.
	XG$putchar$0$0 ==.
	ret
	G$uart_w$0$0 ==.
	C$uart.c$22$1_0$7 ==.
;	uart.c: 22: void uart_w (uint8_t data)
; genLabel
;	-----------------------------------------
;	 function uart_w
;	-----------------------------------------
;	Register assignment is optimal.
;	Stack space usage: 0 bytes.
_uart_w:
	C$uart.c$24$1_0$7 ==.
;	uart.c: 24: while(!(UART->SR & 0x80));
; genLabel
00101$:
; genPointerGet
; genAnd
	ld	a, 0x5230
; peephole 30 removed redundant tnz.
; peephole j5 changed absolute to relative unconditional jump.
	jrpl	00101$
; peephole j8 removed jra by using inverse jump logic
; peephole j30 removed unused label 00116$.
; skipping generated iCode
	C$uart.c$25$1_0$7 ==.
;	uart.c: 25: UART->DR = data;
; genPointerSet
	ldw	x, #0x5231
	ld	a, (0x03, sp)
	ld	(x), a
; genLabel
; peephole j30 removed unused label 00104$.
	C$uart.c$26$1_0$7 ==.
;	uart.c: 26: }
; genEndFunction
	C$uart.c$26$1_0$7 ==.
	XG$uart_w$0$0 ==.
	ret
	G$uart_r$0$0 ==.
	C$uart.c$28$1_0$8 ==.
;	uart.c: 28: uint8_t uart_r ()
; genLabel
;	-----------------------------------------
;	 function uart_r
;	-----------------------------------------
;	Register assignment is optimal.
;	Stack space usage: 0 bytes.
_uart_r:
	C$uart.c$30$1_0$8 ==.
;	uart.c: 30: uint8_t data = UART->DR;
; genPointerGet
	ld	a, 0x5231
	C$uart.c$31$1_0$8 ==.
;	uart.c: 31: return data;
; genReturn
; genLabel
; peephole j30 removed unused label 00101$.
	C$uart.c$32$1_0$8 ==.
;	uart.c: 32: }
; genEndFunction
	C$uart.c$32$1_0$8 ==.
	XG$uart_r$0$0 ==.
	ret
	.area CODE
	.area CONST
	.area INITIALIZER
	.area CABS (ABS)

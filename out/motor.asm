;--------------------------------------------------------
; File Created by SDCC : free open source ANSI-C Compiler
; Version 3.9.0 #11195 (Linux)
;--------------------------------------------------------
	.module motor
	.optsdcc -mstm8
	
;--------------------------------------------------------
; Public variables in this module
;--------------------------------------------------------
	.globl _next_pin
	.globl _delay
	.globl _motor_init
	.globl _motor_step
	.globl _motor_turn
	.globl _motor_stop
;--------------------------------------------------------
; ram data
;--------------------------------------------------------
	.area DATA
;--------------------------------------------------------
; ram data
;--------------------------------------------------------
	.area INITIALIZED
Fmotor$curent_pin$0_0$0==.
_curent_pin:
	.ds 1
Fmotor$pins$0_0$0==.
_pins:
	.ds 4
;--------------------------------------------------------
; absolute external ram data
;--------------------------------------------------------
	.area DABS (ABS)

; default segment ordering for linker
	.area HOME
	.area GSINIT
	.area GSFINAL
	.area CONST
	.area INITIALIZER
	.area CODE

;--------------------------------------------------------
; global & static initialisations
;--------------------------------------------------------
	.area HOME
	.area GSINIT
	.area GSFINAL
	.area GSINIT
;--------------------------------------------------------
; Home
;--------------------------------------------------------
	.area HOME
	.area HOME
;--------------------------------------------------------
; code
;--------------------------------------------------------
	.area CODE
	G$motor_init$0$0 ==.
	C$motor.c$8$0_0$14 ==.
;	motor.c: 8: void motor_init()
; genLabel
;	-----------------------------------------
;	 function motor_init
;	-----------------------------------------
;	Register assignment is optimal.
;	Stack space usage: 0 bytes.
_motor_init:
	C$motor.c$10$1_0$14 ==.
;	motor.c: 10: M_Port->DDR |= MIN_Mask;
; genPointerGet
	ld	a, 0x500c
; genOr
	or	a, #0x78
; genPointerSet
	ld	0x500c, a
	C$motor.c$11$1_0$14 ==.
;	motor.c: 11: M_Port->CR1 |= MIN_Mask;
; genPointerGet
	ld	a, 0x500d
; genOr
	or	a, #0x78
; genPointerSet
	ld	0x500d, a
; genLabel
; peephole j30 removed unused label 00101$.
	C$motor.c$13$1_0$14 ==.
;	motor.c: 13: }
; genEndFunction
	C$motor.c$13$1_0$14 ==.
	XG$motor_init$0$0 ==.
	ret
	G$next_pin$0$0 ==.
	C$motor.c$15$1_0$16 ==.
;	motor.c: 15: uint8_t next_pin(uint8_t pin, int8_t dir)
; genLabel
;	-----------------------------------------
;	 function next_pin
;	-----------------------------------------
;	Register assignment is optimal.
;	Stack space usage: 0 bytes.
_next_pin:
	C$motor.c$17$1_0$16 ==.
;	motor.c: 17: if (dir > 0)
; genCmp
; genCmpTop
	ld	a, (0x04, sp)
	cp	a, #0x00
; peephole j5 changed absolute to relative unconditional jump.
	jrsle	00109$
; peephole j13 removed jra by using inverse jump logic
; peephole j30 removed unused label 00133$.
; skipping generated iCode
	C$motor.c$19$2_0$17 ==.
;	motor.c: 19: pin++;
; genPlus
	inc	(0x03, sp)
	C$motor.c$20$2_0$17 ==.
;	motor.c: 20: if (pin > 4) pin = 1;
; genCmp
; genCmpTop
	ld	a, (0x03, sp)
	cp	a, #0x04
; peephole j5 changed absolute to relative unconditional jump.
	jrule	00110$
; peephole j16 removed jra by using inverse jump logic
; peephole j30 removed unused label 00134$.
; skipping generated iCode
; genAssign
	ld	a, #0x01
	ld	(0x03, sp), a
; genGoto
	jra	00110$
; peephole j5 changed absolute to relative unconditional jump.
; genLabel
00109$:
	C$motor.c$22$1_0$16 ==.
;	motor.c: 22: else if (dir < 0)
; genCmp
; genCmpTop
	tnz	(0x04, sp)
; peephole j5 changed absolute to relative unconditional jump.
	jrpl	00110$
; peephole j8 removed jra by using inverse jump logic
; peephole j30 removed unused label 00135$.
; skipping generated iCode
	C$motor.c$24$2_0$18 ==.
;	motor.c: 24: pin--;
; genMinus
	dec	(0x03, sp)
	C$motor.c$25$2_0$18 ==.
;	motor.c: 25: if (pin < 1 || pin > 4) pin = 4;
; genCmp
; genCmpTop
	ld	a, (0x03, sp)
	cp	a, #0x01
; peephole j5 changed absolute to relative unconditional jump.
	jrc	00103$
; peephole j9 removed jra by using inverse jump logic
; peephole j30 removed unused label 00136$.
; skipping generated iCode
; genCmp
; genCmpTop
	ld	a, (0x03, sp)
	cp	a, #0x04
; peephole j5 changed absolute to relative unconditional jump.
	jrule	00110$
; peephole j16 removed jra by using inverse jump logic
; peephole j30 removed unused label 00137$.
; skipping generated iCode
; genLabel
00103$:
; genAssign
	ld	a, #0x04
	ld	(0x03, sp), a
; genLabel
00110$:
	C$motor.c$27$1_0$16 ==.
;	motor.c: 27: return pin;
; genReturn
	ld	a, (0x03, sp)
; genLabel
; peephole j30 removed unused label 00111$.
	C$motor.c$28$1_0$16 ==.
;	motor.c: 28: }
; genEndFunction
	C$motor.c$28$1_0$16 ==.
	XG$next_pin$0$0 ==.
	ret
	G$motor_step$0$0 ==.
	C$motor.c$46$1_0$20 ==.
;	motor.c: 46: void motor_step(int8_t dir, uint8_t speed)
; genLabel
;	-----------------------------------------
;	 function motor_step
;	-----------------------------------------
;	Register assignment might be sub-optimal.
;	Stack space usage: 5 bytes.
_motor_step:
	sub	sp, #5
	C$motor.c$48$1_0$20 ==.
;	motor.c: 48: int tick = 10000 - speed * 1000;
; genCast
; genAssign
	clrw	x
	ld	a, (0x09, sp)
	ld	xl, a
; genIPush
	pushw	x
; genIPush
	push	#0xe8
	push	#0x03
; genCall
	call	__mulint
	addw	sp, #4
	ldw	(0x04, sp), x
; genMinus
	ldw	y, #0x2710
	subw	y, (0x04, sp)
; genAssign
	C$motor.c$49$1_0$20 ==.
;	motor.c: 49: M_Port->ODR |= pins[curent_pin-1];
; genPointerGet
	ld	a, 0x500a
	ld	(0x05, sp), a
; skipping iCode since result will be rematerialized
; genCast
; genAssign
	ld	a, _curent_pin+0
; genMinus
	dec	a
; genCast
	ld	xl, a
; peephole 4 removed redundant load from xl into a.
	rlc	a
	clr	a
	sbc	a, #0x00
; genPlus
	ld	xh, a
	addw	x, #(_pins + 0)
; genPointerGet
	ld	a, (x)
; genOr
	or	a, (0x05, sp)
; genPointerSet
	ld	0x500a, a
	C$motor.c$50$1_0$20 ==.
;	motor.c: 50: delay(tick);
; genCast
	ldw	(0x03, sp), y
	ld	a, (0x03, sp)
	rlc	a
	clr	a
	sbc	a, #0x00
	ld	(0x02, sp), a
	ld	(0x01, sp), a
; genIPush
	ldw	x, (0x03, sp)
	pushw	x
	ldw	x, (0x03, sp)
	pushw	x
; genCall
	call	_delay
	addw	sp, #4
	C$motor.c$51$1_0$20 ==.
;	motor.c: 51: M_Port->ODR &= ~(pins[next_pin (curent_pin, dir*(-1))-1]);
; genPointerGet
	ld	a, 0x500a
	ld	(0x05, sp), a
; genUminus
	ld	a, (0x08, sp)
	neg	a
; genIPush
	push	a
; genIPush
	push	_curent_pin+0
; genCall
	call	_next_pin
	addw	sp, #2
; genCast
; genAssign
; genMinus
	dec	a
; genCast
	ld	xl, a
; peephole 4 removed redundant load from xl into a.
	rlc	a
	clr	a
	sbc	a, #0x00
; genPlus
	ld	xh, a
	addw	x, #(_pins + 0)
; genPointerGet
	ld	a, (x)
; genCpl
	cpl	a
; genAnd
	and	a, (0x05, sp)
; genPointerSet
	ld	0x500a, a
	C$motor.c$52$1_0$20 ==.
;	motor.c: 52: delay(tick);
; genIPush
	ldw	x, (0x03, sp)
	pushw	x
	ldw	x, (0x03, sp)
	pushw	x
; genCall
	call	_delay
	addw	sp, #4
	C$motor.c$53$1_0$20 ==.
;	motor.c: 53: curent_pin = next_pin (curent_pin, dir);
; genIPush
	ld	a, (0x08, sp)
	push	a
; genIPush
	push	_curent_pin+0
; genCall
	call	_next_pin
	addw	sp, #2
; genAssign
	ld	_curent_pin+0, a
; genLabel
; peephole j30 removed unused label 00101$.
	C$motor.c$91$1_0$20 ==.
;	motor.c: 91: }
; genEndFunction
	addw	sp, #5
	C$motor.c$91$1_0$20 ==.
	XG$motor_step$0$0 ==.
	ret
	G$motor_turn$0$0 ==.
	C$motor.c$93$1_0$22 ==.
;	motor.c: 93: void motor_turn(int steps, int speed)
; genLabel
;	-----------------------------------------
;	 function motor_turn
;	-----------------------------------------
;	Register assignment is optimal.
;	Stack space usage: 0 bytes.
_motor_turn:
	C$motor.c$95$2_0$22 ==.
;	motor.c: 95: int8_t s = 1;
; genAssign
	ld	a, #0x01
	ld	xl, a
	C$motor.c$96$1_0$22 ==.
;	motor.c: 96: if (steps < 0) s = -1;
; genCmp
; genCmpTop
	tnz	(0x03, sp)
; peephole j5 changed absolute to relative unconditional jump.
	jrpl	00111$
; peephole j8 removed jra by using inverse jump logic
; peephole j30 removed unused label 00124$.
; skipping generated iCode
; genAssign
	ld	a, #0xff
	ld	xl, a
	C$motor.c$97$2_0$22 ==.
;	motor.c: 97: for (int i = 0; i != steps; i + s)
; genLabel
00111$:
; genCmpEQorNE
	ldw	y, (0x03, sp)
	jrne	00126$
	ld	a, #0x01
	ld	xh, a
	jra	00127$
; peephole j5 changed absolute to relative unconditional jump.
00126$:
	clr	a
	ld	xh, a
00127$:
; genLabel
00105$:
; genIfx
	ld	a, xh
	tnz	a
; peephole j5 changed absolute to relative unconditional jump.
	jrne	00107$
; peephole j7 removed jra by using inverse jump logic
; peephole j30 removed unused label 00128$.
	C$motor.c$99$3_0$24 ==.
;	motor.c: 99: motor_step(s, speed);
; genCast
; genAssign
	ld	a, (0x06, sp)
; genIPush
	pushw	x
	push	a
; genIPush
	ld	a, xl
	push	a
; genCall
	call	_motor_step
	addw	sp, #2
	popw	x
	C$motor.c$97$2_0$23 ==.
;	motor.c: 97: for (int i = 0; i != steps; i + s)
; genGoto
	jra	00105$
; peephole j5 changed absolute to relative unconditional jump.
; genLabel
00107$:
	C$motor.c$101$2_0$22 ==.
;	motor.c: 101: }
; genEndFunction
	C$motor.c$101$2_0$22 ==.
	XG$motor_turn$0$0 ==.
	ret
	G$motor_stop$0$0 ==.
	C$motor.c$103$2_0$25 ==.
;	motor.c: 103: void motor_stop(){
; genLabel
;	-----------------------------------------
;	 function motor_stop
;	-----------------------------------------
;	Register assignment is optimal.
;	Stack space usage: 0 bytes.
_motor_stop:
	C$motor.c$104$1_0$25 ==.
;	motor.c: 104: M_Port->ODR &= ~(MIN_Mask);
; genPointerGet
	ld	a, 0x500a
; genAnd
	and	a, #0x87
; genPointerSet
	ld	0x500a, a
; genLabel
; peephole j30 removed unused label 00101$.
	C$motor.c$105$1_0$25 ==.
;	motor.c: 105: }
; genEndFunction
	C$motor.c$105$1_0$25 ==.
	XG$motor_stop$0$0 ==.
	ret
	.area CODE
	.area CONST
	.area INITIALIZER
Fmotor$__xinit_curent_pin$0_0$0 == .
__xinit__curent_pin:
	.db #0x01	; 1
Fmotor$__xinit_pins$0_0$0 == .
__xinit__pins:
	.db #0x08	; 8
	.db #0x10	; 16
	.db #0x20	; 32
	.db #0x40	; 64
	.area CABS (ABS)

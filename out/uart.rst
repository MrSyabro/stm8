                                      1 ;--------------------------------------------------------
                                      2 ; File Created by SDCC : free open source ANSI-C Compiler
                                      3 ; Version 3.9.0 #11195 (Linux)
                                      4 ;--------------------------------------------------------
                                      5 	.module uart
                                      6 	.optsdcc -mstm8
                                      7 	
                                      8 ;--------------------------------------------------------
                                      9 ; Public variables in this module
                                     10 ;--------------------------------------------------------
                                     11 	.globl _uart_init
                                     12 	.globl _putchar
                                     13 	.globl _uart_w
                                     14 	.globl _uart_r
                                     15 ;--------------------------------------------------------
                                     16 ; ram data
                                     17 ;--------------------------------------------------------
                                     18 	.area DATA
                                     19 ;--------------------------------------------------------
                                     20 ; ram data
                                     21 ;--------------------------------------------------------
                                     22 	.area INITIALIZED
                                     23 ;--------------------------------------------------------
                                     24 ; absolute external ram data
                                     25 ;--------------------------------------------------------
                                     26 	.area DABS (ABS)
                                     27 
                                     28 ; default segment ordering for linker
                                     29 	.area HOME
                                     30 	.area GSINIT
                                     31 	.area GSFINAL
                                     32 	.area CONST
                                     33 	.area INITIALIZER
                                     34 	.area CODE
                                     35 
                                     36 ;--------------------------------------------------------
                                     37 ; global & static initialisations
                                     38 ;--------------------------------------------------------
                                     39 	.area HOME
                                     40 	.area GSINIT
                                     41 	.area GSFINAL
                                     42 	.area GSINIT
                                     43 ;--------------------------------------------------------
                                     44 ; Home
                                     45 ;--------------------------------------------------------
                                     46 	.area HOME
                                     47 	.area HOME
                                     48 ;--------------------------------------------------------
                                     49 ; code
                                     50 ;--------------------------------------------------------
                                     51 	.area CODE
                           000000    52 	G$uart_init$0$0 ==.
                           000000    53 	C$uart.c$4$0_0$3 ==.
                                     54 ;	uart.c: 4: void uart_init()
                                     55 ; genLabel
                                     56 ;	-----------------------------------------
                                     57 ;	 function uart_init
                                     58 ;	-----------------------------------------
                                     59 ;	Register assignment is optimal.
                                     60 ;	Stack space usage: 0 bytes.
      0080D0                         61 _uart_init:
                           000000    62 	C$uart.c$6$1_0$3 ==.
                                     63 ;	uart.c: 6: CLK->PCKENR1 |= 0b1100;
                                     64 ; genPointerGet
      0080D0 C6 50 C7         [ 1]   65 	ld	a, 0x50c7
                                     66 ; genOr
      0080D3 AA 0C            [ 1]   67 	or	a, #0x0c
                                     68 ; genPointerSet
      0080D5 C7 50 C7         [ 1]   69 	ld	0x50c7, a
                           000008    70 	C$uart.c$8$1_0$3 ==.
                                     71 ;	uart.c: 8: UART->BRR1 = 0x68;
                                     72 ; genPointerSet
      0080D8 35 68 52 32      [ 1]   73 	mov	0x5232+0, #0x68
                           00000C    74 	C$uart.c$9$1_0$3 ==.
                                     75 ;	uart.c: 9: UART->BRR2 = 0x03;
                                     76 ; genPointerSet
      0080DC 35 03 52 33      [ 1]   77 	mov	0x5233+0, #0x03
                           000010    78 	C$uart.c$11$1_0$3 ==.
                                     79 ;	uart.c: 11: UART->CR2 = 0x8;
                                     80 ; genPointerSet
      0080E0 35 08 52 35      [ 1]   81 	mov	0x5235+0, #0x08
                                     82 ; genLabel
                                     83 ; peephole j30 removed unused label 00101$.
                           000014    84 	C$uart.c$12$1_0$3 ==.
                                     85 ;	uart.c: 12: }
                                     86 ; genEndFunction
                           000014    87 	C$uart.c$12$1_0$3 ==.
                           000014    88 	XG$uart_init$0$0 ==.
      0080E4 81               [ 4]   89 	ret
                           000015    90 	G$putchar$0$0 ==.
                           000015    91 	C$uart.c$16$1_0$5 ==.
                                     92 ;	uart.c: 16: int putchar (int data)
                                     93 ; genLabel
                                     94 ;	-----------------------------------------
                                     95 ;	 function putchar
                                     96 ;	-----------------------------------------
                                     97 ;	Register assignment is optimal.
                                     98 ;	Stack space usage: 0 bytes.
      0080E5                         99 _putchar:
                           000015   100 	C$uart.c$18$1_0$5 ==.
                                    101 ;	uart.c: 18: uart_w ((uint8_t)data);
                                    102 ; genCast
                                    103 ; genAssign
      0080E5 7B 04            [ 1]  104 	ld	a, (0x04, sp)
                                    105 ; genIPush
      0080E7 88               [ 1]  106 	push	a
                                    107 ; genCall
      0080E8 CD 80 EF         [ 4]  108 	call	_uart_w
      0080EB 84               [ 1]  109 	pop	a
                           00001C   110 	C$uart.c$19$1_0$5 ==.
                                    111 ;	uart.c: 19: return data;
                                    112 ; genReturn
      0080EC 1E 03            [ 2]  113 	ldw	x, (0x03, sp)
                                    114 ; genLabel
                                    115 ; peephole j30 removed unused label 00101$.
                           00001E   116 	C$uart.c$20$1_0$5 ==.
                                    117 ;	uart.c: 20: }
                                    118 ; genEndFunction
                           00001E   119 	C$uart.c$20$1_0$5 ==.
                           00001E   120 	XG$putchar$0$0 ==.
      0080EE 81               [ 4]  121 	ret
                           00001F   122 	G$uart_w$0$0 ==.
                           00001F   123 	C$uart.c$22$1_0$7 ==.
                                    124 ;	uart.c: 22: void uart_w (uint8_t data)
                                    125 ; genLabel
                                    126 ;	-----------------------------------------
                                    127 ;	 function uart_w
                                    128 ;	-----------------------------------------
                                    129 ;	Register assignment is optimal.
                                    130 ;	Stack space usage: 0 bytes.
      0080EF                        131 _uart_w:
                           00001F   132 	C$uart.c$24$1_0$7 ==.
                                    133 ;	uart.c: 24: while(!(UART->SR & 0x80));
                                    134 ; genLabel
      0080EF                        135 00101$:
                                    136 ; genPointerGet
                                    137 ; genAnd
      0080EF C6 52 30         [ 1]  138 	ld	a, 0x5230
                                    139 ; peephole 30 removed redundant tnz.
                                    140 ; peephole j5 changed absolute to relative unconditional jump.
      0080F2 2A FB            [ 1]  141 	jrpl	00101$
                                    142 ; peephole j8 removed jra by using inverse jump logic
                                    143 ; peephole j30 removed unused label 00116$.
                                    144 ; skipping generated iCode
                           000024   145 	C$uart.c$25$1_0$7 ==.
                                    146 ;	uart.c: 25: UART->DR = data;
                                    147 ; genPointerSet
      0080F4 AE 52 31         [ 2]  148 	ldw	x, #0x5231
      0080F7 7B 03            [ 1]  149 	ld	a, (0x03, sp)
      0080F9 F7               [ 1]  150 	ld	(x), a
                                    151 ; genLabel
                                    152 ; peephole j30 removed unused label 00104$.
                           00002A   153 	C$uart.c$26$1_0$7 ==.
                                    154 ;	uart.c: 26: }
                                    155 ; genEndFunction
                           00002A   156 	C$uart.c$26$1_0$7 ==.
                           00002A   157 	XG$uart_w$0$0 ==.
      0080FA 81               [ 4]  158 	ret
                           00002B   159 	G$uart_r$0$0 ==.
                           00002B   160 	C$uart.c$28$1_0$8 ==.
                                    161 ;	uart.c: 28: uint8_t uart_r ()
                                    162 ; genLabel
                                    163 ;	-----------------------------------------
                                    164 ;	 function uart_r
                                    165 ;	-----------------------------------------
                                    166 ;	Register assignment is optimal.
                                    167 ;	Stack space usage: 0 bytes.
      0080FB                        168 _uart_r:
                           00002B   169 	C$uart.c$30$1_0$8 ==.
                                    170 ;	uart.c: 30: uint8_t data = UART->DR;
                                    171 ; genPointerGet
      0080FB C6 52 31         [ 1]  172 	ld	a, 0x5231
                           00002E   173 	C$uart.c$31$1_0$8 ==.
                                    174 ;	uart.c: 31: return data;
                                    175 ; genReturn
                                    176 ; genLabel
                                    177 ; peephole j30 removed unused label 00101$.
                           00002E   178 	C$uart.c$32$1_0$8 ==.
                                    179 ;	uart.c: 32: }
                                    180 ; genEndFunction
                           00002E   181 	C$uart.c$32$1_0$8 ==.
                           00002E   182 	XG$uart_r$0$0 ==.
      0080FE 81               [ 4]  183 	ret
                                    184 	.area CODE
                                    185 	.area CONST
                                    186 	.area INITIALIZER
                                    187 	.area CABS (ABS)

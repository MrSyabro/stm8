                                      1 ;--------------------------------------------------------
                                      2 ; File Created by SDCC : free open source ANSI-C Compiler
                                      3 ; Version 3.9.0 #11195 (Linux)
                                      4 ;--------------------------------------------------------
                                      5 	.module spi
                                      6 	.optsdcc -mstm8
                                      7 	
                                      8 ;--------------------------------------------------------
                                      9 ; Public variables in this module
                                     10 ;--------------------------------------------------------
                                     11 	.globl _printf
                                     12 	.globl _init
                                     13 	.globl _spi_init
                                     14 	.globl _spi_rw
                                     15 ;--------------------------------------------------------
                                     16 ; ram data
                                     17 ;--------------------------------------------------------
                                     18 	.area DATA
                                     19 ;--------------------------------------------------------
                                     20 ; ram data
                                     21 ;--------------------------------------------------------
                                     22 	.area INITIALIZED
                           000000    23 G$init$0_0$0==.
      000001                         24 _init::
      000001                         25 	.ds 1
                                     26 ;--------------------------------------------------------
                                     27 ; absolute external ram data
                                     28 ;--------------------------------------------------------
                                     29 	.area DABS (ABS)
                                     30 
                                     31 ; default segment ordering for linker
                                     32 	.area HOME
                                     33 	.area GSINIT
                                     34 	.area GSFINAL
                                     35 	.area CONST
                                     36 	.area INITIALIZER
                                     37 	.area CODE
                                     38 
                                     39 ;--------------------------------------------------------
                                     40 ; global & static initialisations
                                     41 ;--------------------------------------------------------
                                     42 	.area HOME
                                     43 	.area GSINIT
                                     44 	.area GSFINAL
                                     45 	.area GSINIT
                                     46 ;--------------------------------------------------------
                                     47 ; Home
                                     48 ;--------------------------------------------------------
                                     49 	.area HOME
                                     50 	.area HOME
                                     51 ;--------------------------------------------------------
                                     52 ; code
                                     53 ;--------------------------------------------------------
                                     54 	.area CODE
                           000000    55 	G$spi_init$0$0 ==.
                           000000    56 	C$spi.c$7$0_0$13 ==.
                                     57 ;	spi.c: 7: void spi_init ()
                                     58 ; genLabel
                                     59 ;	-----------------------------------------
                                     60 ;	 function spi_init
                                     61 ;	-----------------------------------------
                                     62 ;	Register assignment is optimal.
                                     63 ;	Stack space usage: 0 bytes.
      00811F                         64 _spi_init:
                           000000    65 	C$spi.c$9$1_0$13 ==.
                                     66 ;	spi.c: 9: if (!init){
                                     67 ; genIfx
      00811F 72 5D 00 01      [ 1]   68 	tnz	_init+0
                                     69 ; peephole j5 changed absolute to relative unconditional jump.
      008123 26 33            [ 1]   70 	jrne	00111$
                                     71 ; peephole j7 removed jra by using inverse jump logic
                                     72 ; peephole j30 removed unused label 00138$.
                           000006    73 	C$spi.c$10$2_0$14 ==.
                                     74 ;	spi.c: 10: GPIOC->CR1 |= (uint8_t)0x70;
                                     75 ; genPointerGet
      008125 C6 50 0D         [ 1]   76 	ld	a, 0x500d
                                     77 ; genOr
      008128 AA 70            [ 1]   78 	or	a, #0x70
                                     79 ; genPointerSet
      00812A C7 50 0D         [ 1]   80 	ld	0x500d, a
                           00000E    81 	C$spi.c$12$2_0$14 ==.
                                     82 ;	spi.c: 12: CLK->PCKENR1 |= (uint8_t)0x2;
                                     83 ; genPointerGet
                                     84 ; genOr
                                     85 ; genPointerSet
      00812D 72 12 50 C7      [ 1]   86 	bset	20679, #1
                                     87 ; peephole 18-1 replaced or by bset.
                           000012    88 	C$spi.c$13$2_0$14 ==.
                                     89 ;	spi.c: 13: SPI->CR2 = 0x3;
                                     90 ; genPointerSet
      008131 35 03 52 01      [ 1]   91 	mov	0x5201+0, #0x03
                           000016    92 	C$spi.c$14$2_0$14 ==.
                                     93 ;	spi.c: 14: SPI->CR1 = (uint8_t)0x3C;
                                     94 ; genPointerSet
      008135 35 3C 52 00      [ 1]   95 	mov	0x5200+0, #0x3c
                           00001A    96 	C$spi.c$15$2_0$14 ==.
                                     97 ;	spi.c: 15: SPI->CR1 |= (uint8_t)0x40;
                                     98 ; genPointerGet
                                     99 ; genOr
                                    100 ; genPointerSet
      008139 72 1C 52 00      [ 1]  101 	bset	20992, #6
                                    102 ; peephole 18-6 replaced or by bset.
                           00001E   103 	C$spi.c$16$2_0$14 ==.
                                    104 ;	spi.c: 16: while (SPI->SR & (uint8_t)0x80);
                                    105 ; genLabel
      00813D                        106 00101$:
                                    107 ; genPointerGet
                                    108 ; genAnd
      00813D C6 52 03         [ 1]  109 	ld	a, 0x5203
                                    110 ; peephole 30 removed redundant tnz.
                                    111 ; peephole j5 changed absolute to relative unconditional jump.
      008140 2B FB            [ 1]  112 	jrmi	00101$
                                    113 ; peephole j11 removed jra by using inverse jump logic
                                    114 ; peephole j30 removed unused label 00139$.
                                    115 ; skipping generated iCode
                           000023   116 	C$spi.c$17$2_0$14 ==.
                                    117 ;	spi.c: 17: if (SPI->SR & (uint8_t)0x20) // If error
                                    118 ; genPointerGet
      008142 C6 52 03         [ 1]  119 	ld	a, 0x5203
                                    120 ; genAnd
      008145 A5 20            [ 1]  121 	bcp	a, #0x20
                                    122 ; peephole j5 changed absolute to relative unconditional jump.
      008147 27 0B            [ 1]  123 	jreq	00108$
                                    124 ; peephole j10 removed jra by using inverse jump logic
                                    125 ; peephole j30 removed unused label 00140$.
                                    126 ; skipping generated iCode
                           00002A   127 	C$spi.c$19$3_0$15 ==.
                                    128 ;	spi.c: 19: printf("SPI init error\n\r");
                                    129 ; skipping iCode since result will be rematerialized
                                    130 ; skipping iCode since result will be rematerialized
                                    131 ; genIPush
      008149 4B 35            [ 1]  132 	push	#<(___str_0 + 0)
      00814B 4B 80            [ 1]  133 	push	#((___str_0 + 0) >> 8)
                                    134 ; genCall
      00814D CD 81 8C         [ 4]  135 	call	_printf
      008150 5B 02            [ 2]  136 	addw	sp, #2
                           000033   137 	C$spi.c$20$3_0$15 ==.
                                    138 ;	spi.c: 20: while(1);
                                    139 ; genLabel
      008152                        140 00105$:
                                    141 ; genGoto
      008152 20 FE            [ 2]  142 	jra	00105$
                                    143 ; peephole j5 changed absolute to relative unconditional jump.
                                    144 ; genLabel
      008154                        145 00108$:
                           000035   146 	C$spi.c$22$2_0$14 ==.
                                    147 ;	spi.c: 22: init = 1;
                                    148 ; genAssign
      008154 35 01 00 01      [ 1]  149 	mov	_init+0, #0x01
                                    150 ; genLabel
      008158                        151 00111$:
                           000039   152 	C$spi.c$24$1_0$13 ==.
                                    153 ;	spi.c: 24: }
                                    154 ; genEndFunction
                           000039   155 	C$spi.c$24$1_0$13 ==.
                           000039   156 	XG$spi_init$0$0 ==.
      008158 81               [ 4]  157 	ret
                           00003A   158 	G$spi_rw$0$0 ==.
                           00003A   159 	C$spi.c$26$1_0$17 ==.
                                    160 ;	spi.c: 26: uint8_t spi_rw (uint8_t data)
                                    161 ; genLabel
                                    162 ;	-----------------------------------------
                                    163 ;	 function spi_rw
                                    164 ;	-----------------------------------------
                                    165 ;	Register assignment is optimal.
                                    166 ;	Stack space usage: 0 bytes.
      008159                        167 _spi_rw:
                           00003A   168 	C$spi.c$28$1_0$17 ==.
                                    169 ;	spi.c: 28: while (SPI->SR & (uint8_t)0x80);
                                    170 ; genLabel
      008159                        171 00101$:
                                    172 ; genPointerGet
                                    173 ; genAnd
      008159 C6 52 03         [ 1]  174 	ld	a, 0x5203
                                    175 ; peephole 30 removed redundant tnz.
                                    176 ; peephole j5 changed absolute to relative unconditional jump.
      00815C 2B FB            [ 1]  177 	jrmi	00101$
                                    178 ; peephole j11 removed jra by using inverse jump logic
                                    179 ; peephole j30 removed unused label 00129$.
                                    180 ; skipping generated iCode
                           00003F   181 	C$spi.c$29$1_0$17 ==.
                                    182 ;	spi.c: 29: SPI->DR = data;
                                    183 ; genPointerSet
      00815E AE 52 04         [ 2]  184 	ldw	x, #0x5204
      008161 7B 03            [ 1]  185 	ld	a, (0x03, sp)
      008163 F7               [ 1]  186 	ld	(x), a
                           000045   187 	C$spi.c$30$1_0$17 ==.
                                    188 ;	spi.c: 30: while (!(SPI->SR & (uint8_t)0x03));
                                    189 ; genLabel
      008164                        190 00104$:
                                    191 ; genPointerGet
      008164 C6 52 03         [ 1]  192 	ld	a, 0x5203
                                    193 ; genAnd
      008167 A5 03            [ 1]  194 	bcp	a, #0x03
                                    195 ; peephole j5 changed absolute to relative unconditional jump.
      008169 27 F9            [ 1]  196 	jreq	00104$
                                    197 ; peephole j10 removed jra by using inverse jump logic
                                    198 ; peephole j30 removed unused label 00130$.
                                    199 ; skipping generated iCode
                           00004C   200 	C$spi.c$31$1_0$17 ==.
                                    201 ;	spi.c: 31: return SPI->DR;
                                    202 ; genPointerGet
      00816B C6 52 04         [ 1]  203 	ld	a, 0x5204
                                    204 ; genReturn
                                    205 ; genLabel
                                    206 ; peephole j30 removed unused label 00107$.
                           00004F   207 	C$spi.c$32$1_0$17 ==.
                                    208 ;	spi.c: 32: }
                                    209 ; genEndFunction
                           00004F   210 	C$spi.c$32$1_0$17 ==.
                           00004F   211 	XG$spi_rw$0$0 ==.
      00816E 81               [ 4]  212 	ret
                                    213 	.area CODE
                                    214 	.area CONST
                           000000   215 Fspi$__str_0$0_0$0 == .
                                    216 	.area CONST
      008035                        217 ___str_0:
      008035 53 50 49 20 69 6E 69   218 	.ascii "SPI init error"
             74 20 65 72 72 6F 72
      008043 0A                     219 	.db 0x0a
      008044 0D                     220 	.db 0x0d
      008045 00                     221 	.db 0x00
                                    222 	.area CODE
                                    223 	.area INITIALIZER
                           000000   224 Fspi$__xinit_init$0_0$0 == .
      008051                        225 __xinit__init:
      008051 00                     226 	.db #0x00	; 0
                                    227 	.area CABS (ABS)

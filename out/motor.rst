                                      1 ;--------------------------------------------------------
                                      2 ; File Created by SDCC : free open source ANSI-C Compiler
                                      3 ; Version 3.9.0 #11195 (Linux)
                                      4 ;--------------------------------------------------------
                                      5 	.module motor
                                      6 	.optsdcc -mstm8
                                      7 	
                                      8 ;--------------------------------------------------------
                                      9 ; Public variables in this module
                                     10 ;--------------------------------------------------------
                                     11 	.globl _next_pin
                                     12 	.globl _delay
                                     13 	.globl _motor_init
                                     14 	.globl _motor_step
                                     15 	.globl _motor_turn
                                     16 	.globl _motor_stop
                                     17 ;--------------------------------------------------------
                                     18 ; ram data
                                     19 ;--------------------------------------------------------
                                     20 	.area DATA
                                     21 ;--------------------------------------------------------
                                     22 ; ram data
                                     23 ;--------------------------------------------------------
                                     24 	.area INITIALIZED
                           000000    25 Fmotor$curent_pin$0_0$0==.
      000001                         26 _curent_pin:
      000001                         27 	.ds 1
                           000001    28 Fmotor$pins$0_0$0==.
      000002                         29 _pins:
      000002                         30 	.ds 4
                                     31 ;--------------------------------------------------------
                                     32 ; absolute external ram data
                                     33 ;--------------------------------------------------------
                                     34 	.area DABS (ABS)
                                     35 
                                     36 ; default segment ordering for linker
                                     37 	.area HOME
                                     38 	.area GSINIT
                                     39 	.area GSFINAL
                                     40 	.area CONST
                                     41 	.area INITIALIZER
                                     42 	.area CODE
                                     43 
                                     44 ;--------------------------------------------------------
                                     45 ; global & static initialisations
                                     46 ;--------------------------------------------------------
                                     47 	.area HOME
                                     48 	.area GSINIT
                                     49 	.area GSFINAL
                                     50 	.area GSINIT
                                     51 ;--------------------------------------------------------
                                     52 ; Home
                                     53 ;--------------------------------------------------------
                                     54 	.area HOME
                                     55 	.area HOME
                                     56 ;--------------------------------------------------------
                                     57 ; code
                                     58 ;--------------------------------------------------------
                                     59 	.area CODE
                           000000    60 	G$motor_init$0$0 ==.
                           000000    61 	C$motor.c$8$0_0$14 ==.
                                     62 ;	motor.c: 8: void motor_init()
                                     63 ; genLabel
                                     64 ;	-----------------------------------------
                                     65 ;	 function motor_init
                                     66 ;	-----------------------------------------
                                     67 ;	Register assignment is optimal.
                                     68 ;	Stack space usage: 0 bytes.
      008137                         69 _motor_init:
                           000000    70 	C$motor.c$10$1_0$14 ==.
                                     71 ;	motor.c: 10: M_Port->DDR |= MIN_Mask;
                                     72 ; genPointerGet
      008137 C6 50 0C         [ 1]   73 	ld	a, 0x500c
                                     74 ; genOr
      00813A AA 78            [ 1]   75 	or	a, #0x78
                                     76 ; genPointerSet
      00813C C7 50 0C         [ 1]   77 	ld	0x500c, a
                           000008    78 	C$motor.c$11$1_0$14 ==.
                                     79 ;	motor.c: 11: M_Port->CR1 |= MIN_Mask;
                                     80 ; genPointerGet
      00813F C6 50 0D         [ 1]   81 	ld	a, 0x500d
                                     82 ; genOr
      008142 AA 78            [ 1]   83 	or	a, #0x78
                                     84 ; genPointerSet
      008144 C7 50 0D         [ 1]   85 	ld	0x500d, a
                                     86 ; genLabel
                                     87 ; peephole j30 removed unused label 00101$.
                           000010    88 	C$motor.c$13$1_0$14 ==.
                                     89 ;	motor.c: 13: }
                                     90 ; genEndFunction
                           000010    91 	C$motor.c$13$1_0$14 ==.
                           000010    92 	XG$motor_init$0$0 ==.
      008147 81               [ 4]   93 	ret
                           000011    94 	G$next_pin$0$0 ==.
                           000011    95 	C$motor.c$15$1_0$16 ==.
                                     96 ;	motor.c: 15: uint8_t next_pin(uint8_t pin, int8_t dir)
                                     97 ; genLabel
                                     98 ;	-----------------------------------------
                                     99 ;	 function next_pin
                                    100 ;	-----------------------------------------
                                    101 ;	Register assignment is optimal.
                                    102 ;	Stack space usage: 0 bytes.
      008148                        103 _next_pin:
                           000011   104 	C$motor.c$17$1_0$16 ==.
                                    105 ;	motor.c: 17: if (dir > 0)
                                    106 ; genCmp
                                    107 ; genCmpTop
      008148 7B 04            [ 1]  108 	ld	a, (0x04, sp)
      00814A A1 00            [ 1]  109 	cp	a, #0x00
                                    110 ; peephole j5 changed absolute to relative unconditional jump.
      00814C 2D 0E            [ 1]  111 	jrsle	00109$
                                    112 ; peephole j13 removed jra by using inverse jump logic
                                    113 ; peephole j30 removed unused label 00133$.
                                    114 ; skipping generated iCode
                           000017   115 	C$motor.c$19$2_0$17 ==.
                                    116 ;	motor.c: 19: pin++;
                                    117 ; genPlus
      00814E 0C 03            [ 1]  118 	inc	(0x03, sp)
                           000019   119 	C$motor.c$20$2_0$17 ==.
                                    120 ;	motor.c: 20: if (pin > 4) pin = 1;
                                    121 ; genCmp
                                    122 ; genCmpTop
      008150 7B 03            [ 1]  123 	ld	a, (0x03, sp)
      008152 A1 04            [ 1]  124 	cp	a, #0x04
                                    125 ; peephole j5 changed absolute to relative unconditional jump.
      008154 23 1C            [ 2]  126 	jrule	00110$
                                    127 ; peephole j16 removed jra by using inverse jump logic
                                    128 ; peephole j30 removed unused label 00134$.
                                    129 ; skipping generated iCode
                                    130 ; genAssign
      008156 A6 01            [ 1]  131 	ld	a, #0x01
      008158 6B 03            [ 1]  132 	ld	(0x03, sp), a
                                    133 ; genGoto
      00815A 20 16            [ 2]  134 	jra	00110$
                                    135 ; peephole j5 changed absolute to relative unconditional jump.
                                    136 ; genLabel
      00815C                        137 00109$:
                           000025   138 	C$motor.c$22$1_0$16 ==.
                                    139 ;	motor.c: 22: else if (dir < 0)
                                    140 ; genCmp
                                    141 ; genCmpTop
      00815C 0D 04            [ 1]  142 	tnz	(0x04, sp)
                                    143 ; peephole j5 changed absolute to relative unconditional jump.
      00815E 2A 12            [ 1]  144 	jrpl	00110$
                                    145 ; peephole j8 removed jra by using inverse jump logic
                                    146 ; peephole j30 removed unused label 00135$.
                                    147 ; skipping generated iCode
                           000029   148 	C$motor.c$24$2_0$18 ==.
                                    149 ;	motor.c: 24: pin--;
                                    150 ; genMinus
      008160 0A 03            [ 1]  151 	dec	(0x03, sp)
                           00002B   152 	C$motor.c$25$2_0$18 ==.
                                    153 ;	motor.c: 25: if (pin < 1 || pin > 4) pin = 4;
                                    154 ; genCmp
                                    155 ; genCmpTop
      008162 7B 03            [ 1]  156 	ld	a, (0x03, sp)
      008164 A1 01            [ 1]  157 	cp	a, #0x01
                                    158 ; peephole j5 changed absolute to relative unconditional jump.
      008166 25 06            [ 1]  159 	jrc	00103$
                                    160 ; peephole j9 removed jra by using inverse jump logic
                                    161 ; peephole j30 removed unused label 00136$.
                                    162 ; skipping generated iCode
                                    163 ; genCmp
                                    164 ; genCmpTop
      008168 7B 03            [ 1]  165 	ld	a, (0x03, sp)
      00816A A1 04            [ 1]  166 	cp	a, #0x04
                                    167 ; peephole j5 changed absolute to relative unconditional jump.
      00816C 23 04            [ 2]  168 	jrule	00110$
                                    169 ; peephole j16 removed jra by using inverse jump logic
                                    170 ; peephole j30 removed unused label 00137$.
                                    171 ; skipping generated iCode
                                    172 ; genLabel
      00816E                        173 00103$:
                                    174 ; genAssign
      00816E A6 04            [ 1]  175 	ld	a, #0x04
      008170 6B 03            [ 1]  176 	ld	(0x03, sp), a
                                    177 ; genLabel
      008172                        178 00110$:
                           00003B   179 	C$motor.c$27$1_0$16 ==.
                                    180 ;	motor.c: 27: return pin;
                                    181 ; genReturn
      008172 7B 03            [ 1]  182 	ld	a, (0x03, sp)
                                    183 ; genLabel
                                    184 ; peephole j30 removed unused label 00111$.
                           00003D   185 	C$motor.c$28$1_0$16 ==.
                                    186 ;	motor.c: 28: }
                                    187 ; genEndFunction
                           00003D   188 	C$motor.c$28$1_0$16 ==.
                           00003D   189 	XG$next_pin$0$0 ==.
      008174 81               [ 4]  190 	ret
                           00003E   191 	G$motor_step$0$0 ==.
                           00003E   192 	C$motor.c$46$1_0$20 ==.
                                    193 ;	motor.c: 46: void motor_step(int8_t dir, uint8_t speed)
                                    194 ; genLabel
                                    195 ;	-----------------------------------------
                                    196 ;	 function motor_step
                                    197 ;	-----------------------------------------
                                    198 ;	Register assignment might be sub-optimal.
                                    199 ;	Stack space usage: 5 bytes.
      008175                        200 _motor_step:
      008175 52 05            [ 2]  201 	sub	sp, #5
                           000040   202 	C$motor.c$48$1_0$20 ==.
                                    203 ;	motor.c: 48: int tick = 10000 - speed * 1000;
                                    204 ; genCast
                                    205 ; genAssign
      008177 5F               [ 1]  206 	clrw	x
      008178 7B 09            [ 1]  207 	ld	a, (0x09, sp)
      00817A 97               [ 1]  208 	ld	xl, a
                                    209 ; genIPush
      00817B 89               [ 2]  210 	pushw	x
                                    211 ; genIPush
      00817C 4B E8            [ 1]  212 	push	#0xe8
      00817E 4B 03            [ 1]  213 	push	#0x03
                                    214 ; genCall
      008180 CD 82 2C         [ 4]  215 	call	__mulint
      008183 5B 04            [ 2]  216 	addw	sp, #4
      008185 1F 04            [ 2]  217 	ldw	(0x04, sp), x
                                    218 ; genMinus
      008187 90 AE 27 10      [ 2]  219 	ldw	y, #0x2710
      00818B 72 F2 04         [ 2]  220 	subw	y, (0x04, sp)
                                    221 ; genAssign
                           000057   222 	C$motor.c$49$1_0$20 ==.
                                    223 ;	motor.c: 49: M_Port->ODR |= pins[curent_pin-1];
                                    224 ; genPointerGet
      00818E C6 50 0A         [ 1]  225 	ld	a, 0x500a
      008191 6B 05            [ 1]  226 	ld	(0x05, sp), a
                                    227 ; skipping iCode since result will be rematerialized
                                    228 ; genCast
                                    229 ; genAssign
      008193 C6 00 01         [ 1]  230 	ld	a, _curent_pin+0
                                    231 ; genMinus
      008196 4A               [ 1]  232 	dec	a
                                    233 ; genCast
      008197 97               [ 1]  234 	ld	xl, a
                                    235 ; peephole 4 removed redundant load from xl into a.
      008198 49               [ 1]  236 	rlc	a
      008199 4F               [ 1]  237 	clr	a
      00819A A2 00            [ 1]  238 	sbc	a, #0x00
                                    239 ; genPlus
      00819C 95               [ 1]  240 	ld	xh, a
      00819D 1C 00 02         [ 2]  241 	addw	x, #(_pins + 0)
                                    242 ; genPointerGet
      0081A0 F6               [ 1]  243 	ld	a, (x)
                                    244 ; genOr
      0081A1 1A 05            [ 1]  245 	or	a, (0x05, sp)
                                    246 ; genPointerSet
      0081A3 C7 50 0A         [ 1]  247 	ld	0x500a, a
                           00006F   248 	C$motor.c$50$1_0$20 ==.
                                    249 ;	motor.c: 50: delay(tick);
                                    250 ; genCast
      0081A6 17 03            [ 2]  251 	ldw	(0x03, sp), y
      0081A8 7B 03            [ 1]  252 	ld	a, (0x03, sp)
      0081AA 49               [ 1]  253 	rlc	a
      0081AB 4F               [ 1]  254 	clr	a
      0081AC A2 00            [ 1]  255 	sbc	a, #0x00
      0081AE 6B 02            [ 1]  256 	ld	(0x02, sp), a
      0081B0 6B 01            [ 1]  257 	ld	(0x01, sp), a
                                    258 ; genIPush
      0081B2 1E 03            [ 2]  259 	ldw	x, (0x03, sp)
      0081B4 89               [ 2]  260 	pushw	x
      0081B5 1E 03            [ 2]  261 	ldw	x, (0x03, sp)
      0081B7 89               [ 2]  262 	pushw	x
                                    263 ; genCall
      0081B8 CD 80 45         [ 4]  264 	call	_delay
      0081BB 5B 04            [ 2]  265 	addw	sp, #4
                           000086   266 	C$motor.c$51$1_0$20 ==.
                                    267 ;	motor.c: 51: M_Port->ODR &= ~(pins[next_pin (curent_pin, dir*(-1))-1]);
                                    268 ; genPointerGet
      0081BD C6 50 0A         [ 1]  269 	ld	a, 0x500a
      0081C0 6B 05            [ 1]  270 	ld	(0x05, sp), a
                                    271 ; genUminus
      0081C2 7B 08            [ 1]  272 	ld	a, (0x08, sp)
      0081C4 40               [ 1]  273 	neg	a
                                    274 ; genIPush
      0081C5 88               [ 1]  275 	push	a
                                    276 ; genIPush
      0081C6 3B 00 01         [ 1]  277 	push	_curent_pin+0
                                    278 ; genCall
      0081C9 CD 81 48         [ 4]  279 	call	_next_pin
      0081CC 5B 02            [ 2]  280 	addw	sp, #2
                                    281 ; genCast
                                    282 ; genAssign
                                    283 ; genMinus
      0081CE 4A               [ 1]  284 	dec	a
                                    285 ; genCast
      0081CF 97               [ 1]  286 	ld	xl, a
                                    287 ; peephole 4 removed redundant load from xl into a.
      0081D0 49               [ 1]  288 	rlc	a
      0081D1 4F               [ 1]  289 	clr	a
      0081D2 A2 00            [ 1]  290 	sbc	a, #0x00
                                    291 ; genPlus
      0081D4 95               [ 1]  292 	ld	xh, a
      0081D5 1C 00 02         [ 2]  293 	addw	x, #(_pins + 0)
                                    294 ; genPointerGet
      0081D8 F6               [ 1]  295 	ld	a, (x)
                                    296 ; genCpl
      0081D9 43               [ 1]  297 	cpl	a
                                    298 ; genAnd
      0081DA 14 05            [ 1]  299 	and	a, (0x05, sp)
                                    300 ; genPointerSet
      0081DC C7 50 0A         [ 1]  301 	ld	0x500a, a
                           0000A8   302 	C$motor.c$52$1_0$20 ==.
                                    303 ;	motor.c: 52: delay(tick);
                                    304 ; genIPush
      0081DF 1E 03            [ 2]  305 	ldw	x, (0x03, sp)
      0081E1 89               [ 2]  306 	pushw	x
      0081E2 1E 03            [ 2]  307 	ldw	x, (0x03, sp)
      0081E4 89               [ 2]  308 	pushw	x
                                    309 ; genCall
      0081E5 CD 80 45         [ 4]  310 	call	_delay
      0081E8 5B 04            [ 2]  311 	addw	sp, #4
                           0000B3   312 	C$motor.c$53$1_0$20 ==.
                                    313 ;	motor.c: 53: curent_pin = next_pin (curent_pin, dir);
                                    314 ; genIPush
      0081EA 7B 08            [ 1]  315 	ld	a, (0x08, sp)
      0081EC 88               [ 1]  316 	push	a
                                    317 ; genIPush
      0081ED 3B 00 01         [ 1]  318 	push	_curent_pin+0
                                    319 ; genCall
      0081F0 CD 81 48         [ 4]  320 	call	_next_pin
      0081F3 5B 02            [ 2]  321 	addw	sp, #2
                                    322 ; genAssign
      0081F5 C7 00 01         [ 1]  323 	ld	_curent_pin+0, a
                                    324 ; genLabel
                                    325 ; peephole j30 removed unused label 00101$.
                           0000C1   326 	C$motor.c$91$1_0$20 ==.
                                    327 ;	motor.c: 91: }
                                    328 ; genEndFunction
      0081F8 5B 05            [ 2]  329 	addw	sp, #5
                           0000C3   330 	C$motor.c$91$1_0$20 ==.
                           0000C3   331 	XG$motor_step$0$0 ==.
      0081FA 81               [ 4]  332 	ret
                           0000C4   333 	G$motor_turn$0$0 ==.
                           0000C4   334 	C$motor.c$93$1_0$22 ==.
                                    335 ;	motor.c: 93: void motor_turn(int steps, int speed)
                                    336 ; genLabel
                                    337 ;	-----------------------------------------
                                    338 ;	 function motor_turn
                                    339 ;	-----------------------------------------
                                    340 ;	Register assignment is optimal.
                                    341 ;	Stack space usage: 0 bytes.
      0081FB                        342 _motor_turn:
                           0000C4   343 	C$motor.c$95$2_0$22 ==.
                                    344 ;	motor.c: 95: int8_t s = 1;
                                    345 ; genAssign
      0081FB A6 01            [ 1]  346 	ld	a, #0x01
      0081FD 97               [ 1]  347 	ld	xl, a
                           0000C7   348 	C$motor.c$96$1_0$22 ==.
                                    349 ;	motor.c: 96: if (steps < 0) s = -1;
                                    350 ; genCmp
                                    351 ; genCmpTop
      0081FE 0D 03            [ 1]  352 	tnz	(0x03, sp)
                                    353 ; peephole j5 changed absolute to relative unconditional jump.
      008200 2A 03            [ 1]  354 	jrpl	00111$
                                    355 ; peephole j8 removed jra by using inverse jump logic
                                    356 ; peephole j30 removed unused label 00124$.
                                    357 ; skipping generated iCode
                                    358 ; genAssign
      008202 A6 FF            [ 1]  359 	ld	a, #0xff
      008204 97               [ 1]  360 	ld	xl, a
                           0000CE   361 	C$motor.c$97$2_0$22 ==.
                                    362 ;	motor.c: 97: for (int i = 0; i != steps; i + s)
                                    363 ; genLabel
      008205                        364 00111$:
                                    365 ; genCmpEQorNE
      008205 16 03            [ 2]  366 	ldw	y, (0x03, sp)
      008207 26 05            [ 1]  367 	jrne	00126$
      008209 A6 01            [ 1]  368 	ld	a, #0x01
      00820B 95               [ 1]  369 	ld	xh, a
      00820C 20 02            [ 2]  370 	jra	00127$
                                    371 ; peephole j5 changed absolute to relative unconditional jump.
      00820E                        372 00126$:
      00820E 4F               [ 1]  373 	clr	a
      00820F 95               [ 1]  374 	ld	xh, a
      008210                        375 00127$:
                                    376 ; genLabel
      008210                        377 00105$:
                                    378 ; genIfx
      008210 9E               [ 1]  379 	ld	a, xh
      008211 4D               [ 1]  380 	tnz	a
                                    381 ; peephole j5 changed absolute to relative unconditional jump.
      008212 26 0E            [ 1]  382 	jrne	00107$
                                    383 ; peephole j7 removed jra by using inverse jump logic
                                    384 ; peephole j30 removed unused label 00128$.
                           0000DD   385 	C$motor.c$99$3_0$24 ==.
                                    386 ;	motor.c: 99: motor_step(s, speed);
                                    387 ; genCast
                                    388 ; genAssign
      008214 7B 06            [ 1]  389 	ld	a, (0x06, sp)
                                    390 ; genIPush
      008216 89               [ 2]  391 	pushw	x
      008217 88               [ 1]  392 	push	a
                                    393 ; genIPush
      008218 9F               [ 1]  394 	ld	a, xl
      008219 88               [ 1]  395 	push	a
                                    396 ; genCall
      00821A CD 81 75         [ 4]  397 	call	_motor_step
      00821D 5B 02            [ 2]  398 	addw	sp, #2
      00821F 85               [ 2]  399 	popw	x
                           0000E9   400 	C$motor.c$97$2_0$23 ==.
                                    401 ;	motor.c: 97: for (int i = 0; i != steps; i + s)
                                    402 ; genGoto
      008220 20 EE            [ 2]  403 	jra	00105$
                                    404 ; peephole j5 changed absolute to relative unconditional jump.
                                    405 ; genLabel
      008222                        406 00107$:
                           0000EB   407 	C$motor.c$101$2_0$22 ==.
                                    408 ;	motor.c: 101: }
                                    409 ; genEndFunction
                           0000EB   410 	C$motor.c$101$2_0$22 ==.
                           0000EB   411 	XG$motor_turn$0$0 ==.
      008222 81               [ 4]  412 	ret
                           0000EC   413 	G$motor_stop$0$0 ==.
                           0000EC   414 	C$motor.c$103$2_0$25 ==.
                                    415 ;	motor.c: 103: void motor_stop(){
                                    416 ; genLabel
                                    417 ;	-----------------------------------------
                                    418 ;	 function motor_stop
                                    419 ;	-----------------------------------------
                                    420 ;	Register assignment is optimal.
                                    421 ;	Stack space usage: 0 bytes.
      008223                        422 _motor_stop:
                           0000EC   423 	C$motor.c$104$1_0$25 ==.
                                    424 ;	motor.c: 104: M_Port->ODR &= ~(MIN_Mask);
                                    425 ; genPointerGet
      008223 C6 50 0A         [ 1]  426 	ld	a, 0x500a
                                    427 ; genAnd
      008226 A4 87            [ 1]  428 	and	a, #0x87
                                    429 ; genPointerSet
      008228 C7 50 0A         [ 1]  430 	ld	0x500a, a
                                    431 ; genLabel
                                    432 ; peephole j30 removed unused label 00101$.
                           0000F4   433 	C$motor.c$105$1_0$25 ==.
                                    434 ;	motor.c: 105: }
                                    435 ; genEndFunction
                           0000F4   436 	C$motor.c$105$1_0$25 ==.
                           0000F4   437 	XG$motor_stop$0$0 ==.
      00822B 81               [ 4]  438 	ret
                                    439 	.area CODE
                                    440 	.area CONST
                                    441 	.area INITIALIZER
                           000000   442 Fmotor$__xinit_curent_pin$0_0$0 == .
      008040                        443 __xinit__curent_pin:
      008040 01                     444 	.db #0x01	; 1
                           000001   445 Fmotor$__xinit_pins$0_0$0 == .
      008041                        446 __xinit__pins:
      008041 08                     447 	.db #0x08	; 8
      008042 10                     448 	.db #0x10	; 16
      008043 20                     449 	.db #0x20	; 32
      008044 40                     450 	.db #0x40	; 64
                                    451 	.area CABS (ABS)

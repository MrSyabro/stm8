;--------------------------------------------------------
; File Created by SDCC : free open source ANSI-C Compiler
; Version 3.9.0 #11195 (Linux)
;--------------------------------------------------------
	.module spi
	.optsdcc -mstm8
	
;--------------------------------------------------------
; Public variables in this module
;--------------------------------------------------------
	.globl _printf
	.globl _init
	.globl _spi_init
	.globl _spi_rw
;--------------------------------------------------------
; ram data
;--------------------------------------------------------
	.area DATA
;--------------------------------------------------------
; ram data
;--------------------------------------------------------
	.area INITIALIZED
G$init$0_0$0==.
_init::
	.ds 1
;--------------------------------------------------------
; absolute external ram data
;--------------------------------------------------------
	.area DABS (ABS)

; default segment ordering for linker
	.area HOME
	.area GSINIT
	.area GSFINAL
	.area CONST
	.area INITIALIZER
	.area CODE

;--------------------------------------------------------
; global & static initialisations
;--------------------------------------------------------
	.area HOME
	.area GSINIT
	.area GSFINAL
	.area GSINIT
;--------------------------------------------------------
; Home
;--------------------------------------------------------
	.area HOME
	.area HOME
;--------------------------------------------------------
; code
;--------------------------------------------------------
	.area CODE
	G$spi_init$0$0 ==.
	C$spi.c$7$0_0$13 ==.
;	spi.c: 7: void spi_init ()
; genLabel
;	-----------------------------------------
;	 function spi_init
;	-----------------------------------------
;	Register assignment is optimal.
;	Stack space usage: 0 bytes.
_spi_init:
	C$spi.c$9$1_0$13 ==.
;	spi.c: 9: if (!init){
; genIfx
	tnz	_init+0
; peephole j5 changed absolute to relative unconditional jump.
	jrne	00111$
; peephole j7 removed jra by using inverse jump logic
; peephole j30 removed unused label 00138$.
	C$spi.c$10$2_0$14 ==.
;	spi.c: 10: GPIOC->CR1 |= (uint8_t)0x70;
; genPointerGet
	ld	a, 0x500d
; genOr
	or	a, #0x70
; genPointerSet
	ld	0x500d, a
	C$spi.c$12$2_0$14 ==.
;	spi.c: 12: CLK->PCKENR1 |= (uint8_t)0x2;
; genPointerGet
; genOr
; genPointerSet
	bset	20679, #1
; peephole 18-1 replaced or by bset.
	C$spi.c$13$2_0$14 ==.
;	spi.c: 13: SPI->CR2 = 0x3;
; genPointerSet
	mov	0x5201+0, #0x03
	C$spi.c$14$2_0$14 ==.
;	spi.c: 14: SPI->CR1 = (uint8_t)0x3C;
; genPointerSet
	mov	0x5200+0, #0x3c
	C$spi.c$15$2_0$14 ==.
;	spi.c: 15: SPI->CR1 |= (uint8_t)0x40;
; genPointerGet
; genOr
; genPointerSet
	bset	20992, #6
; peephole 18-6 replaced or by bset.
	C$spi.c$16$2_0$14 ==.
;	spi.c: 16: while (SPI->SR & (uint8_t)0x80);
; genLabel
00101$:
; genPointerGet
; genAnd
	ld	a, 0x5203
; peephole 30 removed redundant tnz.
; peephole j5 changed absolute to relative unconditional jump.
	jrmi	00101$
; peephole j11 removed jra by using inverse jump logic
; peephole j30 removed unused label 00139$.
; skipping generated iCode
	C$spi.c$17$2_0$14 ==.
;	spi.c: 17: if (SPI->SR & (uint8_t)0x20) // If error
; genPointerGet
	ld	a, 0x5203
; genAnd
	bcp	a, #0x20
; peephole j5 changed absolute to relative unconditional jump.
	jreq	00108$
; peephole j10 removed jra by using inverse jump logic
; peephole j30 removed unused label 00140$.
; skipping generated iCode
	C$spi.c$19$3_0$15 ==.
;	spi.c: 19: printf("SPI init error\n\r");
; skipping iCode since result will be rematerialized
; skipping iCode since result will be rematerialized
; genIPush
	push	#<(___str_0 + 0)
	push	#((___str_0 + 0) >> 8)
; genCall
	call	_printf
	addw	sp, #2
	C$spi.c$20$3_0$15 ==.
;	spi.c: 20: while(1);
; genLabel
00105$:
; genGoto
	jra	00105$
; peephole j5 changed absolute to relative unconditional jump.
; genLabel
00108$:
	C$spi.c$22$2_0$14 ==.
;	spi.c: 22: init = 1;
; genAssign
	mov	_init+0, #0x01
; genLabel
00111$:
	C$spi.c$24$1_0$13 ==.
;	spi.c: 24: }
; genEndFunction
	C$spi.c$24$1_0$13 ==.
	XG$spi_init$0$0 ==.
	ret
	G$spi_rw$0$0 ==.
	C$spi.c$26$1_0$17 ==.
;	spi.c: 26: uint8_t spi_rw (uint8_t data)
; genLabel
;	-----------------------------------------
;	 function spi_rw
;	-----------------------------------------
;	Register assignment is optimal.
;	Stack space usage: 0 bytes.
_spi_rw:
	C$spi.c$28$1_0$17 ==.
;	spi.c: 28: while (SPI->SR & (uint8_t)0x80);
; genLabel
00101$:
; genPointerGet
; genAnd
	ld	a, 0x5203
; peephole 30 removed redundant tnz.
; peephole j5 changed absolute to relative unconditional jump.
	jrmi	00101$
; peephole j11 removed jra by using inverse jump logic
; peephole j30 removed unused label 00129$.
; skipping generated iCode
	C$spi.c$29$1_0$17 ==.
;	spi.c: 29: SPI->DR = data;
; genPointerSet
	ldw	x, #0x5204
	ld	a, (0x03, sp)
	ld	(x), a
	C$spi.c$30$1_0$17 ==.
;	spi.c: 30: while (!(SPI->SR & (uint8_t)0x03));
; genLabel
00104$:
; genPointerGet
	ld	a, 0x5203
; genAnd
	bcp	a, #0x03
; peephole j5 changed absolute to relative unconditional jump.
	jreq	00104$
; peephole j10 removed jra by using inverse jump logic
; peephole j30 removed unused label 00130$.
; skipping generated iCode
	C$spi.c$31$1_0$17 ==.
;	spi.c: 31: return SPI->DR;
; genPointerGet
	ld	a, 0x5204
; genReturn
; genLabel
; peephole j30 removed unused label 00107$.
	C$spi.c$32$1_0$17 ==.
;	spi.c: 32: }
; genEndFunction
	C$spi.c$32$1_0$17 ==.
	XG$spi_rw$0$0 ==.
	ret
	.area CODE
	.area CONST
Fspi$__str_0$0_0$0 == .
	.area CONST
___str_0:
	.ascii "SPI init error"
	.db 0x0a
	.db 0x0d
	.db 0x00
	.area CODE
	.area INITIALIZER
Fspi$__xinit_init$0_0$0 == .
__xinit__init:
	.db #0x00	; 0
	.area CABS (ABS)

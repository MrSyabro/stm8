;--------------------------------------------------------
; File Created by SDCC : free open source ANSI-C Compiler
; Version 3.9.0 #11195 (Linux)
;--------------------------------------------------------
	.module main
	.optsdcc -mstm8
	
;--------------------------------------------------------
; Public variables in this module
;--------------------------------------------------------
	.globl _main
	.globl _sr_wout
	.globl _sr_init
	.globl _uart_init
	.globl _printf
	.globl _delay
;--------------------------------------------------------
; ram data
;--------------------------------------------------------
	.area DATA
;--------------------------------------------------------
; ram data
;--------------------------------------------------------
	.area INITIALIZED
;--------------------------------------------------------
; Stack segment in internal ram 
;--------------------------------------------------------
	.area	SSEG
__start__stack:
	.ds	1

;--------------------------------------------------------
; absolute external ram data
;--------------------------------------------------------
	.area DABS (ABS)

; default segment ordering for linker
	.area HOME
	.area GSINIT
	.area GSFINAL
	.area CONST
	.area INITIALIZER
	.area CODE

;--------------------------------------------------------
; interrupt vector 
;--------------------------------------------------------
	.area HOME
__interrupt_vect:
	int s_GSINIT ; reset
;--------------------------------------------------------
; global & static initialisations
;--------------------------------------------------------
	.area HOME
	.area GSINIT
	.area GSFINAL
	.area GSINIT
__sdcc_gs_init_startup:
__sdcc_init_data:
; stm8_genXINIT() start
	ldw x, #l_DATA
	jreq	00002$
00001$:
	clr (s_DATA - 1, x)
	decw x
	jrne	00001$
00002$:
	ldw	x, #l_INITIALIZER
	jreq	00004$
00003$:
	ld	a, (s_INITIALIZER - 1, x)
	ld	(s_INITIALIZED - 1, x), a
	decw	x
	jrne	00003$
00004$:
; stm8_genXINIT() end
	.area GSFINAL
	jp	__sdcc_program_startup
;--------------------------------------------------------
; Home
;--------------------------------------------------------
	.area HOME
	.area HOME
__sdcc_program_startup:
	jp	_main
;	return from main will return to caller
;--------------------------------------------------------
; code
;--------------------------------------------------------
	.area CODE
	G$delay$0$0 ==.
	C$main.c$9$0_0$17 ==.
;	main.c: 9: void delay(uint32_t t)
; genLabel
;	-----------------------------------------
;	 function delay
;	-----------------------------------------
;	Register assignment might be sub-optimal.
;	Stack space usage: 8 bytes.
_delay:
	sub	sp, #8
	C$main.c$11$1_0$17 ==.
;	main.c: 11: while(t--) {}
; genAssign
	ldw	y, (0x0d, sp)
	ldw	(0x07, sp), y
	ldw	y, (0x0b, sp)
	ldw	(0x05, sp), y
; genLabel
00101$:
; genAssign
	ldw	x, (0x07, sp)
	ldw	(0x03, sp), x
	ldw	x, (0x05, sp)
	ldw	(0x01, sp), x
; genMinus
	ldw	y, (0x07, sp)
	subw	y, #0x0001
	ldw	(0x07, sp), y
	ld	a, (0x06, sp)
	sbc	a, #0x00
	ld	(0x06, sp), a
	ld	a, (0x05, sp)
	sbc	a, #0x00
	ld	(0x05, sp), a
; genIfx
	ldw	x, (0x03, sp)
	jrne	00101$
; peephole j22 jumped to 00101$ directly instead of via 00112$.
	ldw	x, (0x01, sp)
; peephole j30 removed unused label 00112$.
; peephole j5 changed absolute to relative unconditional jump.
	jrne	00101$
; peephole j7 removed jra by using inverse jump logic
; peephole j30 removed unused label 00113$.
; genLabel
; peephole j30 removed unused label 00104$.
	C$main.c$12$1_0$17 ==.
;	main.c: 12: }
; genEndFunction
	addw	sp, #8
	C$main.c$12$1_0$17 ==.
	XG$delay$0$0 ==.
	ret
	G$main$0$0 ==.
	C$main.c$14$1_0$20 ==.
;	main.c: 14: int main(void)
; genLabel
;	-----------------------------------------
;	 function main
;	-----------------------------------------
;	Register assignment is optimal.
;	Stack space usage: 0 bytes.
_main:
	C$main.c$16$1_0$20 ==.
;	main.c: 16: CLK->CKDIVR = 0x0;
; genPointerSet
	mov	0x50c6+0, #0x00
	C$main.c$18$1_0$20 ==.
;	main.c: 18: GPIOB->DDR |= (1 << 5);
; genPointerGet
; genOr
; genPointerSet
	bset	20487, #5
; peephole 18-5 replaced or by bset.
	C$main.c$20$1_0$20 ==.
;	main.c: 20: uart_init ();
; genCall
	call	_uart_init
	C$main.c$21$1_0$20 ==.
;	main.c: 21: printf("UART init: OK!\n\r");
; skipping iCode since result will be rematerialized
; skipping iCode since result will be rematerialized
; genIPush
	push	#<(___str_0 + 0)
	push	#((___str_0 + 0) >> 8)
; genCall
	call	_printf
	addw	sp, #2
	C$main.c$23$1_0$20 ==.
;	main.c: 23: delay(10000UL);
; genIPush
	push	#0x10
	push	#0x27
	clrw	x
	pushw	x
; genCall
	call	_delay
	addw	sp, #4
	C$main.c$49$1_0$20 ==.
;	main.c: 49: sr_init ();
; genCall
	call	_sr_init
	C$main.c$50$1_0$20 ==.
;	main.c: 50: sr_wout (0b0001);
; genIPush
	push	#0x01
; genCall
	call	_sr_wout
	pop	a
	C$main.c$52$1_0$20 ==.
;	main.c: 52: while(1)
; genLabel
00102$:
	C$main.c$54$2_0$21 ==.
;	main.c: 54: GPIOB->ODR |= (1 << 5);
; genPointerGet
; genOr
; genPointerSet
	bset	20485, #5
; peephole 18-5 replaced or by bset.
	C$main.c$55$2_0$21 ==.
;	main.c: 55: delay(100000UL);
; genIPush
	push	#0xa0
	push	#0x86
	push	#0x01
	push	#0x00
; genCall
	call	_delay
	addw	sp, #4
	C$main.c$56$2_0$21 ==.
;	main.c: 56: GPIOB->ODR &= ~(1 << 5);
; genPointerGet
; genAnd
; genPointerSet
	bres	20485, #5
; peephole 19-5 replaced and by bres.
	C$main.c$57$2_0$21 ==.
;	main.c: 57: delay(100000UL);
; genIPush
	push	#0xa0
	push	#0x86
	push	#0x01
	push	#0x00
; genCall
	call	_delay
	addw	sp, #4
; genGoto
	jra	00102$
; peephole j5 changed absolute to relative unconditional jump.
; genLabel
; peephole j30 removed unused label 00104$.
	C$main.c$59$1_0$20 ==.
;	main.c: 59: }
; genEndFunction
	C$main.c$59$1_0$20 ==.
	XG$main$0$0 ==.
	ret
	.area CODE
	.area CONST
Fmain$__str_0$0_0$0 == .
	.area CONST
___str_0:
	.ascii "UART init: OK!"
	.db 0x0a
	.db 0x0d
	.db 0x00
	.area CODE
	.area INITIALIZER
	.area CABS (ABS)

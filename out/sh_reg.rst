                                      1 ;--------------------------------------------------------
                                      2 ; File Created by SDCC : free open source ANSI-C Compiler
                                      3 ; Version 3.9.0 #11195 (Linux)
                                      4 ;--------------------------------------------------------
                                      5 	.module sh_reg
                                      6 	.optsdcc -mstm8
                                      7 	
                                      8 ;--------------------------------------------------------
                                      9 ; Public variables in this module
                                     10 ;--------------------------------------------------------
                                     11 	.globl _spi_rw
                                     12 	.globl _spi_init
                                     13 	.globl _sr_init
                                     14 	.globl _sr_wout
                                     15 ;--------------------------------------------------------
                                     16 ; ram data
                                     17 ;--------------------------------------------------------
                                     18 	.area DATA
                                     19 ;--------------------------------------------------------
                                     20 ; ram data
                                     21 ;--------------------------------------------------------
                                     22 	.area INITIALIZED
                                     23 ;--------------------------------------------------------
                                     24 ; absolute external ram data
                                     25 ;--------------------------------------------------------
                                     26 	.area DABS (ABS)
                                     27 
                                     28 ; default segment ordering for linker
                                     29 	.area HOME
                                     30 	.area GSINIT
                                     31 	.area GSFINAL
                                     32 	.area CONST
                                     33 	.area INITIALIZER
                                     34 	.area CODE
                                     35 
                                     36 ;--------------------------------------------------------
                                     37 ; global & static initialisations
                                     38 ;--------------------------------------------------------
                                     39 	.area HOME
                                     40 	.area GSINIT
                                     41 	.area GSFINAL
                                     42 	.area GSINIT
                                     43 ;--------------------------------------------------------
                                     44 ; Home
                                     45 ;--------------------------------------------------------
                                     46 	.area HOME
                                     47 	.area HOME
                                     48 ;--------------------------------------------------------
                                     49 ; code
                                     50 ;--------------------------------------------------------
                                     51 	.area CODE
                           000000    52 	G$sr_init$0$0 ==.
                           000000    53 	C$sh_reg.c$3$0_0$3 ==.
                                     54 ;	sh_reg.c: 3: void sr_init()
                                     55 ; genLabel
                                     56 ;	-----------------------------------------
                                     57 ;	 function sr_init
                                     58 ;	-----------------------------------------
                                     59 ;	Register assignment is optimal.
                                     60 ;	Stack space usage: 0 bytes.
      0080FF                         61 _sr_init:
                           000000    62 	C$sh_reg.c$5$1_0$3 ==.
                                     63 ;	sh_reg.c: 5: SR_CS_Port->DDR |= SR_CS_Pin;
                                     64 ; genPointerGet
                                     65 ; genOr
                                     66 ; genPointerSet
      0080FF 72 12 50 11      [ 1]   67 	bset	20497, #1
                                     68 ; peephole 18-1 replaced or by bset.
                           000004    69 	C$sh_reg.c$6$1_0$3 ==.
                                     70 ;	sh_reg.c: 6: SR_CS_Port->CR2 |= SR_CS_Pin;
                                     71 ; genPointerGet
                                     72 ; genOr
                                     73 ; genPointerSet
      008103 72 12 50 13      [ 1]   74 	bset	20499, #1
                                     75 ; peephole 18-1 replaced or by bset.
                           000008    76 	C$sh_reg.c$7$1_0$3 ==.
                                     77 ;	sh_reg.c: 7: SR_CS_Port->CR1 |= SR_CS_Pin;
                                     78 ; genPointerGet
                                     79 ; genOr
                                     80 ; genPointerSet
      008107 72 12 50 12      [ 1]   81 	bset	20498, #1
                                     82 ; peephole 18-1 replaced or by bset.
                           00000C    83 	C$sh_reg.c$8$1_0$3 ==.
                                     84 ;	sh_reg.c: 8: spi_init ();
                                     85 ; genCall
      00810B CD 81 1F         [ 4]   86 	call	_spi_init
                                     87 ; genLabel
                                     88 ; peephole j30 removed unused label 00101$.
                           00000F    89 	C$sh_reg.c$9$1_0$3 ==.
                                     90 ;	sh_reg.c: 9: }
                                     91 ; genEndFunction
                           00000F    92 	C$sh_reg.c$9$1_0$3 ==.
                           00000F    93 	XG$sr_init$0$0 ==.
      00810E 81               [ 4]   94 	ret
                           000010    95 	G$sr_wout$0$0 ==.
                           000010    96 	C$sh_reg.c$11$1_0$5 ==.
                                     97 ;	sh_reg.c: 11: void sr_wout(uint8_t out)
                                     98 ; genLabel
                                     99 ;	-----------------------------------------
                                    100 ;	 function sr_wout
                                    101 ;	-----------------------------------------
                                    102 ;	Register assignment is optimal.
                                    103 ;	Stack space usage: 0 bytes.
      00810F                        104 _sr_wout:
                           000010   105 	C$sh_reg.c$13$1_0$5 ==.
                                    106 ;	sh_reg.c: 13: spi_rw(out);
                                    107 ; genIPush
      00810F 7B 03            [ 1]  108 	ld	a, (0x03, sp)
      008111 88               [ 1]  109 	push	a
                                    110 ; genCall
      008112 CD 81 59         [ 4]  111 	call	_spi_rw
      008115 84               [ 1]  112 	pop	a
                           000017   113 	C$sh_reg.c$14$1_0$5 ==.
                                    114 ;	sh_reg.c: 14: H_CS;
                                    115 ; genPointerGet
                                    116 ; genOr
                                    117 ; genPointerSet
      008116 72 12 50 0F      [ 1]  118 	bset	20495, #1
                                    119 ; peephole 18-1 replaced or by bset.
                           00001B   120 	C$sh_reg.c$15$1_0$5 ==.
                                    121 ;	sh_reg.c: 15: L_CS;
                                    122 ; genPointerGet
                                    123 ; genAnd
                                    124 ; genPointerSet
      00811A 72 13 50 0F      [ 1]  125 	bres	20495, #1
                                    126 ; peephole 19-1 replaced and by bres.
                                    127 ; genLabel
                                    128 ; peephole j30 removed unused label 00101$.
                           00001F   129 	C$sh_reg.c$16$1_0$5 ==.
                                    130 ;	sh_reg.c: 16: }
                                    131 ; genEndFunction
                           00001F   132 	C$sh_reg.c$16$1_0$5 ==.
                           00001F   133 	XG$sr_wout$0$0 ==.
      00811E 81               [ 4]  134 	ret
                                    135 	.area CODE
                                    136 	.area CONST
                                    137 	.area INITIALIZER
                                    138 	.area CABS (ABS)

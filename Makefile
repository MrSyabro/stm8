all: out/gpiostm8.hex

CC = sdcc
LD = sdld
ARGS = -mstm8 -DSTM8S103 --std-sdcc2x -DSTM8S \
	--all-callee-saves --debug --stack-auto --fverbose-asm \
	--float-reent

out/gpiostm8.hex: out/main.rel out/uart.rel out/sh_reg.rel out/spi.rel
	$(CC) $^ -o $@ $(ARGS)

out/spi.rel: spi.c spi.h rc522.h periph.h
	$(CC) -c spi.c -o out/ $(ARGS)
	
out/main.rel: main.c spi.h rc522.h periph.h uart.h main.h sh_reg.h
	$(CC) -c main.c -o out/ $(ARGS)
	
out/rc522.rel: rc522.c rc522.h spi.h periph.h
	$(CC) -c rc522.c -o out/ $(ARGS)

out/uart.rel: uart.c periph.h uart.h
	$(CC) -c uart.c -o out/ $(ARGS)
	
out/i2c.rel: i2c.c periph.h i2c.h
	$(CC) -c i2c.c -o out/ $(ARGS)
	
out/sh_reg.rel: sh_reg.c periph.h sh_reg.h
	$(CC) -c sh_reg.c -o out/ $(ARGS)

out/motor.rel: motor.c periph.h motor.h
	$(CC) -c motor.c -o out/ $(ARGS)

clean:
	rm -f out/*

# Builder will call this to install the application before running.
install: 
	echo "Noup."

# Builder uses this target to run your application.
run: out/gpiostm8.hex
	/usr/local/bin/stm8flash -c stlinkv2 -p stm8s103f3 -w $^


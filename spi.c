#include "main.h"
#include "spi.h"
#include "periph.h"

uint8_t init = 0;

void spi_init ()
{
	if (!init){
		GPIOC->CR1 |= (uint8_t)0x70;
		
		CLK->PCKENR1 |= (uint8_t)0x2;
		SPI->CR2 = 0x3;
		SPI->CR1 = (uint8_t)0x3C;
		SPI->CR1 |= (uint8_t)0x40;
		while (SPI->SR & (uint8_t)0x80);
		if (SPI->SR & (uint8_t)0x20) // If error
		{
			printf("SPI init error\n\r");
			while(1);
		}
		init = 1;
	}
}

uint8_t spi_rw (uint8_t data)
{
	while (SPI->SR & (uint8_t)0x80);
	SPI->DR = data;
	while (!(SPI->SR & (uint8_t)0x03));
	return SPI->DR;
}

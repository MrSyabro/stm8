#include "main.h"
//#include "spi.h"
//#include "rc522.h"
#include "periph.h"
#include "uart.h"
#include "sh_reg.h"
//#include "motor.h"

void delay(uint32_t t)
{
	while(t--) {}
}

int main(void)
{
	CLK->CKDIVR = 0x0;
	
	GPIOB->DDR |= (1 << 5);
	
	uart_init ();
	printf("UART init: OK!\n\r");
	
	delay(10000UL);
	/* motor_init (); */
	/* for (uint32_t i = 500; i; i--) */
	/* { */
	/* 	motor_step(1, 9); */
	/* } */
	/* motor_stop (); */
	/* for (uint32_t i = 500; i; i--) */
	/* { */
	/* 	motor_step(-1, 9); */
	/* } */
	/* motor_stop (); */
	
	
	//GPIOD->DDR |= 0x1;
	//GPIOA->CR1 |= 0x4;
	//GPIOD->ODR |= 0x1;
	
	//rc_init ();
	//uint8_t test[4] = {0x37, 0x37, 0xE, 0x00U};
	//for (unsigned int i = 0; i<4; i++)
	//{
	//	uint8_t data = rc_read_reg (test[i]);
	//	printf("0x%X = 0x%X\n\r", test[i], data);
	//}
	
	sr_init ();
	sr_wout (0b0001);
		
	while(1)
	{
		GPIOB->ODR |= (1 << 5);
		delay(100000UL);
		GPIOB->ODR &= ~(1 << 5);
		delay(100000UL);
	}
}



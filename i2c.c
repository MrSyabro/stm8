#include "periph.h"
#include "i2c.h"

void i2c_init()
{
	CLK->PCKENR1 |= 0x1;
	
	I2C->FREQR = 16;
	I2C->CCRH = 0;
	I2C->CCRL = 80;
	I2C->OARH = 0x40;
	I2C->CR1 = 0x1;
}

void send_ack (char nack)
{
	if (nack)
		I2C->CR2 &= ~4;
	else
		I2C->CR2 |= 4;
}

void i2c_send_adr(uint8_t adr)
{
	I2C->DR = adr;
	while(I2C->SR1 & 2); I2C->SR1 &= ~2;
}

void i2c_start ()
{
	send_ack (0);
	I2C->CR2 |= 1;
	while(I2C->SR1 & 1); I2C->SR1 &= ~1;
}
void i2c_stop ()
{
	I2C->CR2 |= 2;
}

void i2c_send (uint8_t data)
{
	I2C->DR = data;
	while(I2C->SR1 & 0x80); I2C->SR1 &= ~0x80;
}

uint8_t i2c_recv ()
{
	send_ack (0);
	I2C->SR1 &= ~2;
	I2C->SR3 &= ~1;
	while(I2C->SR1 & 0x40);
	I2C->SR1 &= ~0x40;
	return I2C->DR;
	send_ack (1);
}

#include <stdint.h>
#include "spi.h"
#include "periph.h"

#ifndef SH_REG_H
#define SH_REG_H

#define SR_CS_Pin 0x2
#define SR_CS_Port GPIOD

#define L_CS SR_CS_Port->ODR &= ~SR_CS_Pin
#define H_CS SR_CS_Port->ODR |= SR_CS_Pin

void sr_init();
void sr_wout(uint8_t out);

#endif
